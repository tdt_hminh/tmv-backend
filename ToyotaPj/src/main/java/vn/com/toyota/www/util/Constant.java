package vn.com.toyota.www.util;

public interface Constant {
	
	final String PACK_ROOT_DIR = "vn.com.toyota.www.*";
	
	public interface RESPONSE_STATUS{
		final int OK = 200;
		final int CREATED = 201;
		final int ACCEPTED = 202;
		final int BAD_REQUEST = 400;
		final int AUTHEN_FAIL = 401;
		final int FORBIDDEN = 403;
		final int RESOURCE_NOT_FOUND = 404;
		final int METHOD_NOT_ALLOWED = 405;
		final int CONFLICT = 409;
		final int PRECONDITION_FAILED = 412;
		final int REQUEST_ENTITY_TOO_LARGE = 413;
		final int INTERNAL_SERVER_ERROR = 500;
		final int NOT_IMPLEMENTED = 501;
		final int SERVICE_UNAVAIABLE = 503;
	}
	
	public interface PAGINATION {
		final int DEFAULT_PAGE = 1;
		final int DEFAULT_PAGE_SIZE = 20;
	}
	
	public interface DATE_TIME_FORMAT {
		final String DDMMYYYY_HHMMSS = "yyyy-MM-dd HH:mm:ss";
		final String HHmmssDDMMYYYY = "HH:mm:ss dd/MM/yyyy";
		final String DDMMYYYY = "dd/MM/yyyy";
		final String EXPORT_DATE = "ddMMyyyy_HHmmss";
		final String YYYYMMDD = "yyyyMMdd";
	}	
	
	public interface ROLE{
		final String ADMIN = "admin";
		final String MEMBER = "member";
		final String HEAD = "head";
		final String ACCOUTING = "accouting";
	}
	public interface SYS_AUTH{
		final String RU = "RU";
		final String R = "R";
	}
	public interface ACCOUNT_TYPE{
		final int BASIC = 1;
		final int ADVANCE = 2;
	}
	public interface FLAG{
		final Short IS_ACTIVE = 1;
		final Short NOT_ACTIVE = 0;
	}

}
