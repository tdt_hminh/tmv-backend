package vn.com.toyota.www.entity;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;

@Entity
@Table(name = "Users", schema = "dbo", catalog = "TMSSBK")
public class UserPO implements Serializable {

	private static final long serialVersionUID = 5998773322154544577L;
	private int userId;
	@NotNull
	private String username;
	@NotNull
	private String displayName;
	@Email
	private String email;
	@Size(max = 4) 
	@NotNull
	private String source;
	@NotNull
	private String passwordHash;
	@NotNull
	private String passwordSalt;
	private Timestamp lastDirectoryUpdate;
	private String userImage;
	private Timestamp insertDate;
	@NotNull
	private int insertUserId;
	private Timestamp updateDate;
	private Integer updateUserId;
	private short isActive;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "UserId")
	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	@Basic
	@Column(name = "Username")
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	@Basic
	@Column(name = "DisplayName")
	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	@Basic
	@Column(name = "Email")
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Basic
	@Column(name = "Source")
	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	@Basic
	@Column(name = "PasswordHash")
	public String getPasswordHash() {
		return passwordHash;
	}

	public void setPasswordHash(String passwordHash) {
		this.passwordHash = passwordHash;
	}

	@Basic
	@Column(name = "PasswordSalt")
	public String getPasswordSalt() {
		return passwordSalt;
	}

	public void setPasswordSalt(String passwordSalt) {
		this.passwordSalt = passwordSalt;
	}

	@Basic
	@Column(name = "LastDirectoryUpdate")
	public Timestamp getLastDirectoryUpdate() {
		return lastDirectoryUpdate;
	}

	public void setLastDirectoryUpdate(Timestamp lastDirectoryUpdate) {
		this.lastDirectoryUpdate = lastDirectoryUpdate;
	}

	@Basic
	@Column(name = "UserImage")
	public String getUserImage() {
		return userImage;
	}

	public void setUserImage(String userImage) {
		this.userImage = userImage;
	}

	@Basic
	@Column(name = "InsertDate")
	public Timestamp getInsertDate() {
		return insertDate;
	}

	public void setInsertDate(Timestamp insertDate) {
		this.insertDate = insertDate;
	}

	@Basic
	@Column(name = "InsertUserId")
	public int getInsertUserId() {
		return insertUserId;
	}

	public void setInsertUserId(int insertUserId) {
		this.insertUserId = insertUserId;
	}

	@Basic
	@Column(name = "UpdateDate")
	public Timestamp getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Timestamp updateDate) {
		this.updateDate = updateDate;
	}

	@Basic
	@Column(name = "UpdateUserId")
	public Integer getUpdateUserId() {
		return updateUserId;
	}

	public void setUpdateUserId(Integer updateUserId) {
		this.updateUserId = updateUserId;
	}

	@Basic
	@Column(name = "IsActive")
	public short getIsActive() {
		return isActive;
	}

	public void setIsActive(short isActive) {
		this.isActive = isActive;
	}

	@Override
	public String toString() {
		return "User [userId=" + userId + ", username=" + username + ", displayName=" + displayName + ", email=" + email
				+ ", source=" + source + ", passwordHash=" + passwordHash + ", passwordSalt=" + passwordSalt
				+ ", lastDirectoryUpdate=" + lastDirectoryUpdate + ", userImage=" + userImage + ", insertDate="
				+ insertDate + ", insertUserId=" + insertUserId + ", updateDate=" + updateDate + ", updateUserId="
				+ updateUserId + ", isActive=" + isActive + "]";
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		UserPO usersPO = (UserPO) o;
		return userId == usersPO.userId && insertUserId == usersPO.insertUserId && isActive == usersPO.isActive
				&& Objects.equals(username, usersPO.username) && Objects.equals(displayName, usersPO.displayName)
				&& Objects.equals(email, usersPO.email) && Objects.equals(source, usersPO.source)
				&& Objects.equals(passwordHash, usersPO.passwordHash)
				&& Objects.equals(passwordSalt, usersPO.passwordSalt)
				&& Objects.equals(lastDirectoryUpdate, usersPO.lastDirectoryUpdate)
				&& Objects.equals(userImage, usersPO.userImage) && Objects.equals(insertDate, usersPO.insertDate)
				&& Objects.equals(updateDate, usersPO.updateDate) && Objects.equals(updateUserId, usersPO.updateUserId);
	}

	@Override
	public int hashCode() {

		return Objects.hash(userId, username, displayName, email, source, passwordHash, passwordSalt,
				lastDirectoryUpdate, userImage, insertDate, insertUserId, updateDate, updateUserId, isActive);
	}
}
