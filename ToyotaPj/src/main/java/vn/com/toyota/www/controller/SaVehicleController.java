package vn.com.toyota.www.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import io.swagger.annotations.ApiOperation;
import vn.com.toyota.www.entity.SaVehiclePO;
import vn.com.toyota.www.model.ResponseModel;
import vn.com.toyota.www.service.SaVehicleService;
import vn.com.toyota.www.util.CommonUtils;

@Controller
public class SaVehicleController {
	/** The logger. */
	Logger LOGGER = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	SaVehicleService saService;
	
	@ApiOperation(value = "Find all active sa-vehicle")
	@RequestMapping(value = "/sa-vehicle/active", method = RequestMethod.GET)
	public ResponseEntity<ResponseModel<Page<SaVehiclePO>>> getAllActiveSaVehicle(
			@RequestParam(value="page", required=false) Integer page,
			@RequestParam(value="size", required=false) Integer size,
			@RequestParam(value="direction", required=false) Integer direction,
			@RequestParam(value="properties", required=false) String properties
			) throws Exception {
		LOGGER.info("Get list of all sa-vehicles");
		ResponseModel<Page<SaVehiclePO>> response = new ResponseModel<Page<SaVehiclePO>>();
		Pageable pag = CommonUtils.createPageable(page, size, direction, properties);
		response.setData(saService.getAllActiveSaVehicle(pag));
		LOGGER.info("Get list of all sa-vehicles success " + response.getData());
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
}
