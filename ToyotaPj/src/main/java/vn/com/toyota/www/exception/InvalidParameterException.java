package vn.com.toyota.www.exception;

public class InvalidParameterException extends Exception {
	
	private static final long serialVersionUID = 165441765319097410L;
	
	private String errorCode;

	public InvalidParameterException() {
	}

	public InvalidParameterException(String message) {
		super(message);
	}

	public InvalidParameterException(Throwable cause) {
		super(cause);
	}

	public InvalidParameterException(String message, Throwable cause) {
		super(message, cause);
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	
	
}
