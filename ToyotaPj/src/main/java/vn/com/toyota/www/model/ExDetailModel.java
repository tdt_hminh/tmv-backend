package vn.com.toyota.www.model;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeName;

@JsonTypeName("error")
@JsonTypeInfo(include= JsonTypeInfo.As.WRAPPER_OBJECT,use= JsonTypeInfo.Id.NAME)
public class ExDetailModel {
	
	public ExDetailModel(){}
	
	public ExDetailModel(String statusCode, String errorCode, String errorMessage) {
		this.status = statusCode;
		this.code = errorCode;
		this.message = errorMessage;
	}

	private String status;
	
	private String code;
	
	private String message;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	

	
	
}
