package vn.com.toyota.www.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import vn.com.toyota.www.entity.SaVehiclePO;


@Repository
public interface SaVehicleRepository extends JpaRepository<SaVehiclePO, Long>{

}
