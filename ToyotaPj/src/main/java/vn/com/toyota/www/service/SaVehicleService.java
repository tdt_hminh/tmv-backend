package vn.com.toyota.www.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import vn.com.toyota.www.entity.SaVehiclePO;
import vn.com.toyota.www.repository.SaVehicleRepository;

@Service
@Transactional
public class SaVehicleService {

	@Autowired
	SaVehicleRepository saVRepository;
	
	public Page<SaVehiclePO> getAllActiveSaVehicle(Pageable page) {
		return saVRepository.findAll(page);
	}

}
