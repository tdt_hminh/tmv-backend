package vn.com.toyota.www.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import vn.com.toyota.www.entity.UserPO;
import vn.com.toyota.www.exception.InvalidParameterException;
import vn.com.toyota.www.exception.ResourceNotFoundException;
import vn.com.toyota.www.exception.ToyotaException;
import vn.com.toyota.www.repository.UserRepository;
import vn.com.toyota.www.util.Constant;

@Service
@Transactional
public class UserService {
	
	Logger LOGGER = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	UserRepository userRepository;


	public Page<UserPO> getAllUser(Pageable pageRequest) {
		return userRepository.findUsers(pageRequest);
	}
	
	public List<UserPO> getAllActiveUser() {
		return userRepository.findAllActiveUser();
	}

	public UserPO getUserInfo(Integer userId) {
		return userRepository.findOneById(userId);
	}
	
	public UserPO addUser(UserPO user) throws InvalidParameterException {
		//ValidationUtils.validateNewUser(user);
		user.setInsertDate(new Timestamp(System.currentTimeMillis()));
		user.setIsActive(Constant.FLAG.IS_ACTIVE);
		return userRepository.save(user);
	}

	public UserPO deleteUser(Integer userId) throws ResourceNotFoundException {
		UserPO user = userRepository.findOneById(userId);
		if (user==null) {
			ResourceNotFoundException ex = new ResourceNotFoundException("User[id="+user.getUserId()+"] not found");
			LOGGER.error(ex.getMessage(), ex);
			throw ex;
		}
		user.setIsActive(Constant.FLAG.NOT_ACTIVE);
		return userRepository.saveAndFlush(user);
	}

	public UserPO updateUser(UserPO user) throws ResourceNotFoundException, InvalidParameterException {
		//ValidationUtils.validateUpdatedUser(user);
		UserPO existedUser = userRepository.findOneById(user.getUserId());
		if (existedUser==null) {
			ResourceNotFoundException ex = new ResourceNotFoundException("User[id="+user.getUserId()+"] not found");
			LOGGER.error(ex.getMessage(),ex);
			throw ex;
		}
		user.setIsActive(Constant.FLAG.IS_ACTIVE);
		userRepository.saveAndFlush(user);
		return existedUser;
	}

	public List<UserPO> updateUserList(List<UserPO> users) throws ResourceNotFoundException, ToyotaException {
		List<UserPO> result = new ArrayList<UserPO>();
		for (UserPO userPO : users) {
			UserPO user = userRepository.findOne(userPO.getUserId());
			if (user==null) 
				throw new RuntimeException("Cannot find user[id="+userPO.getUserId()+"]");
			userPO.setIsActive(Constant.FLAG.IS_ACTIVE);
			userRepository.saveAndFlush(userPO);
			result.add(user);
		}
		return result;
	}

}
