package vn.com.toyota.www.repository.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import vn.com.toyota.www.entity.UserPO;
import vn.com.toyota.www.repository.UserRepositoryCustom;
import vn.com.toyota.www.util.Constant;

public class UserRepositoryImpl implements UserRepositoryCustom {

	Logger LOGGER = LoggerFactory.getLogger(this.getClass());
	
	@PersistenceContext
	private EntityManager entityManager;
	
	@SuppressWarnings("unchecked")
	@Override
	public List<UserPO> findAllActiveUser() {
		String hql = "FROM UserPO WHERE isActive = ?"; 
		return entityManager.createQuery(hql).setParameter(1, Constant.FLAG.IS_ACTIVE).getResultList();
	}

}
