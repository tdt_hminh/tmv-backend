package vn.com.toyota.www.repository;

import java.util.List;

import vn.com.toyota.www.entity.UserPO;
import vn.com.toyota.www.exception.ResourceNotFoundException;
import vn.com.toyota.www.exception.ToyotaException;

public interface UserRepositoryCustom {
	List<UserPO> findAllActiveUser();
}
