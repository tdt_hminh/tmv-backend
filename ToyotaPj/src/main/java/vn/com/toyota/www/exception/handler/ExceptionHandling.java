package vn.com.toyota.www.exception.handler;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import vn.com.toyota.www.exception.InvalidParameterException;
import vn.com.toyota.www.exception.ResourceNotFoundException;
import vn.com.toyota.www.exception.ToyotaException;
import vn.com.toyota.www.model.ExDetailModel;

@ControllerAdvice
@RestController
public class ExceptionHandling extends ResponseEntityExceptionHandler {

	@ExceptionHandler(Exception.class)
	public ResponseEntity<?> exception(Exception e) {
		return new ResponseEntity<>(
				new ExDetailModel(HttpStatus.INTERNAL_SERVER_ERROR.toString(), null, e.getMessage()),
				HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@ExceptionHandler(InvalidParameterException.class)
	public ResponseEntity<?> invalidParamEx(InvalidParameterException e) {
		return new ResponseEntity<>(
				new ExDetailModel(HttpStatus.BAD_REQUEST.toString(), e.getErrorCode(), e.getMessage()),
				HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(ResourceNotFoundException.class)
	public ResponseEntity<?> resourceNotFoundEx(ResourceNotFoundException e) {
		return new ResponseEntity<>(
				new ExDetailModel(HttpStatus.NOT_FOUND.toString(), e.getErrorCode(), e.getMessage()),
				HttpStatus.NOT_FOUND);
	}

	@ExceptionHandler(ToyotaException.class)
	public ResponseEntity<?> toyotaException(ToyotaException e) {
		return new ResponseEntity<>(
				new ExDetailModel(HttpStatus.INTERNAL_SERVER_ERROR.toString(), e.getErrorCode(), e.getMessage()),
				HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		BindingResult bindResult = ex.getBindingResult();
		if (bindResult == null || bindResult.getFieldError() == null)
			return new ResponseEntity<>(new ExDetailModel(HttpStatus.BAD_REQUEST.toString(), null, ex.getMessage()),
					HttpStatus.BAD_REQUEST);
		else {
			String message = bindResult.getFieldError().getField() + " " + bindResult.getFieldError().getDefaultMessage();
			return new ResponseEntity<>(new ExDetailModel(HttpStatus.BAD_REQUEST.toString(), null, message),
					HttpStatus.BAD_REQUEST);
		}

	}

}
