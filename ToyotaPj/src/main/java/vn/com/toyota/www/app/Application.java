package vn.com.toyota.www.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import vn.com.toyota.www.util.Constant;


@SpringBootApplication
@EnableAutoConfiguration
@EnableJpaRepositories(Constant.PACK_ROOT_DIR)
@ComponentScan(Constant.PACK_ROOT_DIR)
@EntityScan(Constant.PACK_ROOT_DIR)  
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);

	}

}
