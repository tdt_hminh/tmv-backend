package vn.com.toyota.www.exception;

public class ToyotaException extends Exception {

	private static final long serialVersionUID = -2719319145659758910L;

	private String errorCode;
	
	public ToyotaException() {
	}

	public ToyotaException(String message) {
		super(message);
	}

	public ToyotaException(Throwable cause) {
		super(cause);
	}

	public ToyotaException(String message, Throwable cause) {
		super(message, cause);
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	
	
}
