package vn.com.toyota.www.util;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.util.StringUtils;

public class CommonUtils {

	public static Direction findDirection(Integer direction){
		if (direction==null || direction==1)
			return Direction.ASC;
		else 
			return Direction.DESC;
	}
	
	public static Pageable createPageable(Integer page, Integer size, Integer direction, String prop){
		if (page==null || page < 0) page = 0;
		if (size==null || size <= 0) size = 20;
			
		if (StringUtils.isEmpty(prop))
			return (Pageable) new PageRequest(page, size);
		else 
			return (Pageable) new PageRequest(page, size, findDirection(direction), prop);
	}
}
