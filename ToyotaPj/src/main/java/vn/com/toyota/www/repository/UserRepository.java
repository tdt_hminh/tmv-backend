package vn.com.toyota.www.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import vn.com.toyota.www.entity.UserPO;

@Repository
public interface UserRepository extends JpaRepository<UserPO, Integer>, UserRepositoryCustom{
	
	@Query("from UserPO where userId = ? and isActive = 1")
	UserPO findOneById(Integer userId);
	
	@Query(value="from UserPO a where a.isActive = 1",
			countQuery="select count(*) from UserPO a where a.isActive = 1")
	Page<UserPO> findUsers(Pageable pageRequest);
}
