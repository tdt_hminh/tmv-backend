package vn.com.toyota.www.controller;

import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import vn.com.toyota.www.entity.UserPO;
import vn.com.toyota.www.model.ExDetailModel;
import vn.com.toyota.www.model.ResponseModel;
import vn.com.toyota.www.service.UserService;
import vn.com.toyota.www.util.CommonUtils;

/**
 * The Class UserController.
 */
@Controller
public class UserController {
	
	/** The logger. */
	Logger LOGGER = LoggerFactory.getLogger(this.getClass());
	
	/** The user service. */
	@Autowired
	UserService userService;
	

	@ApiOperation(value = "Find all active users")
	@RequestMapping(value = "/user", method = RequestMethod.GET)
	public ResponseEntity<ResponseModel<Page<UserPO>>> getAllActiveUser(
			@RequestParam(value="page", required=false) Integer page,
			@RequestParam(value="size", required=false) Integer size,
			@RequestParam(value="direction", required=false) Integer direction,
			@RequestParam(value="properties", required=false) String properties
			) throws Exception {
		LOGGER.info("Get list of all users");
		ResponseModel<Page<UserPO>> response = new ResponseModel<Page<UserPO>>();
		Pageable pag = CommonUtils.createPageable(page, size, direction, properties);
		response.setData(userService.getAllUser(pag));
		LOGGER.info("Get list of all users success " + response.getData());
		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	@ApiOperation(value = "Find user by id")
	@RequestMapping(value = "/user/{user_id}", method = RequestMethod.GET)
	public ResponseEntity<ResponseModel<UserPO>> getUserInfo(
			@PathVariable(value="user_id") Integer userId) throws Exception {
		LOGGER.info("Get information of user");
		ResponseModel<UserPO> response = new ResponseModel<UserPO>();
		response.setData(userService.getUserInfo(userId));
		LOGGER.info("Get information of user success " + response.getData());
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
	@ApiOperation(value = "Create new user")
	@ApiResponses(value = {
			@ApiResponse(code = 400, message = "Invalid parameter of user ", response = ExDetailModel.class),
	    })
	@RequestMapping(value = "/user", method = RequestMethod.PUT)
	public ResponseEntity<ResponseModel<UserPO>> addUser(
			@Valid @RequestBody UserPO user) throws Exception {
		LOGGER.info("Create user");
		ResponseModel<UserPO> response = new ResponseModel<UserPO>();
		response.setData(userService.addUser(user));
		LOGGER.info("Create user success " + response.getData());
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
	@ApiOperation(value = "Delete user by id")
	@ApiResponses(value = {
	        @ApiResponse(code = 404, message = "User not found", response = ExDetailModel.class),
	    })
	@RequestMapping(value = "/user/{user_id}", method = RequestMethod.DELETE)
	public ResponseEntity<ResponseModel<UserPO>> deleteUser(
			@PathVariable(value="user_id") Integer userId) throws Exception {
		LOGGER.info("Delete user [id=" + userId+"]");
		ResponseModel<UserPO> response = new ResponseModel<UserPO>();
		response.setData(userService.deleteUser(userId));
		LOGGER.info("Delete user success " + response.getData());
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
	@ApiOperation(value = "Update user")
	@ApiResponses(value = {
			@ApiResponse(code = 400, message = "Invalid parameter of user ", response = ExDetailModel.class),
	        @ApiResponse(code = 404, message = "User not found", response = ExDetailModel.class),
	    })
	@RequestMapping(value = "/user", method = RequestMethod.POST)
	public ResponseEntity<ResponseModel<UserPO>> updateUser(
			@RequestBody UserPO user) throws Exception {
		LOGGER.info("Update user");
		ResponseModel<UserPO> response = new ResponseModel<UserPO>();
		response.setData(userService.updateUser(user));
		LOGGER.info("Update user success " + response.getData());
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
	@ApiOperation(value = "Update list of users")
	@ApiResponses(value = {
			@ApiResponse(code = 400, message = "Invalid parameter of user ", response = ExDetailModel.class),
	        @ApiResponse(code = 404, message = "User not found", response = ExDetailModel.class),
	        @ApiResponse(code = 500, message = "Exception occur", response = ExDetailModel.class),
	    })
	@RequestMapping(value = "/user/list", method = RequestMethod.POST)
	public ResponseEntity<ResponseModel<List<UserPO>>> updateUserList(
			@RequestBody List<UserPO> user) throws Exception {
		LOGGER.info("Update users");
		ResponseModel<List<UserPO>> response = new ResponseModel<List<UserPO>>();
		response.setData(userService.updateUserList(user));
		LOGGER.info("Update users success " + response.getData());
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
}
