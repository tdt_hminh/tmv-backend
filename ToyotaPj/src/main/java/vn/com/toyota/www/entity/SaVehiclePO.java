package vn.com.toyota.www.entity;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "SA_VEHICLE", schema = "dbo", catalog = "TMSSBK")
public class SaVehiclePO {
    private long id;
    private Long dlrId;
    private String frameNo;
    private String vin;
    private String engineNo;
    private Long accId;
    private Long cfuId;
    private Long graId;
    private Long locId;
    private Long truId;
    private Date installDate;
    private Date painInDate;
    private String ccrLotNo;
    private String ccrNoInLot;
    private String ccrBodyNo;
    private String ccrAIn;
    private String ccrAOut;
    private String keyCode;
    private String qcDriverNo;
    private String qcPassengerNo;
    private String qcEngno;
    private String qcTranmissionNo;
    private String qcAxleNo;
    private String qcLoshift;
    private Date lineOffDate;
    private Long assChangeFrom;
    private String assChangeReason;
    private Date assAlloMonth;
    private Date dlrPaymentPlan;
    private Date rFPaymentDate;
    private String payInvoiceNo;
    private Date payActualInvoiceDate;
    private Long payVnAmount;
    private Long payUsdAmount;
    private Long payHoldbackAmount;
    private Long payRFPayback;
    private Long payDiscountAmount;
    private String documentTmvSituationId;
    private Date documentDeliveryDate;
    private String documentDeliveryTime;
    private Date documentArrivalDate;
    private String documentArrivalTime;
    private String documentArrivalRemark;
    private String documentDlrSituationId;
    private Long fleetPrice;
    private Date mlDeliveryDate;
    private String mlDeliveryTime;
    private Long mlYardAreaId;
    private Long mlYardLocId;
    private String mlLicience;
    private Long mlLogId;
    private Long mlTruckTypeId;
    private Long mlTruckId;
    private Long mlOtherDlr;
    private Date pdiPlanArrivalDate;
    private String pdiPlanArrivalTime;
    private Date pdiArrivalDate;
    private String pdiArrivalTime;
    private String pdiDelayReason;
    private String pdiVehicleSituationId;
    private Long pdiYardArea;
    private Long pdiLocId;
    private Date pdiDeliveryDate;
    private String pdiDeliveryTime;
    private Long pdiLogId;
    private Long pdiTructTypeId;
    private Long pdiTructId;
    private Long pdiTransCost;
    private Date pdiStockMaintenanceDate;
    private Date mlArrivalPlanDate;
    private String mlArrivalPlanTime;
    private Date mlArrivalActualDate;
    private String mlArrivalActualTime;
    private String mlArrivalDelayReason;
    private String mlArrivalSituationId;
    private Long mlArrivalTransCost;
    private Date dlrPlanArrivalDate;
    private String dlrPlanArrivalTime;
    private Date dlrArrivalDate;
    private String dlrArrivalTime;
    private String dlrDelayReason;
    private Long dlrTransCost;
    private String dlrHasFloorMat;
    private Long dlrGasoline;
    private Long dlrYardAreaId;
    private Long dlrYardLocId;
    private String damageIs;
    private Long damageTypeId;
    private Long damageRepaireCost;
    private String damagePlace;
    private String damageReason;
    private String damageDes;
    private Long damageInsCompanyId;
    private Date damageClaimDate;
    private Date damageFinishingDate;
    private Date stockServiceMaintenanceDate;
    private String stockRemark;
    private Long createdBy;
    private Date createDate;
    private Long modifiedBy;
    private Date modifyDate;
    private String dlrReceive;
    private String dlrRemark;
    private String dlrStatus;
    private Date mlPlanDeliveryDate;
    private String mlPlanDeliveryTime;
    private String pdiPlanDeliveryTime;
    private Date pdiPlanDeliveryDate;
    private String dlrVehicleSituationId;
    private Date assignmentDate;
    private String paymentBy;
    private Long graProId;
    private String transportRoute;
    private String isExport;
    private String seqNo;
    private Date pdiConducting;
    private Long fapId;
    private String fleetCustomer;
    private Date vrDate;
    private Date cbuDocument;
    private String isReport;
    private String tfsProcess;
    private Long salesDlrId;
    private Date salesDate;
    private String salesStatus;
    private Long tfsAmount;
    private Date pInstallDate;
    private String pInstallTime;
    private String installTime;
    private Date deferPayment;
    private Date pdiDate;
    private String cbuLocation;
    private Date cbuDocPlan;
    private Date stDelivery;
    private String pioStatus;
    private Date firstTimeMaintPlan;
    private Date firstTimeMaintAct;
    private Date secondTimeMaintPlan;
    private Date secondTimeMaintAct;
    private Timestamp thirdTimeMaintPlan;
    private Date thirdTimeMaintAct;
    private Date forthTimeMaintPlan;
    private Date forthTimeMaintAct;
    private Date fifthTimeMaintPlan;
    private Date fifthTimeMaintAct;
    private Date sixthTimeMaintPlan;
    private Date sixthTimeMaintAct;
    private String pioLackList;
    private Date stArrival;
    private Long stLogId;
    private String paintInTime;
    private Date clDelivery;
    private Long clLogId;
    private Long clTruckTypeId;
    private Long clTruckId;
    private Long spctTruckId;
    private Long spctTruckTypeId;
    private Long spctLogId;
    private Date spctDeliveryDate;
    private String tmapPaperInvoiceNo;
    private Date tmapPaperInvoiceDate;
    private Date customStartPlan;
    private Date vrCheckPlan;
    private Date customCompletePlan;
    private Date customStartAct;
    private Date vrCheckAct;
    private Date customCompleteAct;
    private Long customActLt;
    private Date coqReceiveDate;
    private String homologation;
    private Long marineModeId;
    private Long departPortId;
    private String arivalPortId;
    private Date portEtd;
    private Date portEta;
    private Date actDepartDate;
    private Date actArrivalDate;
    private Long vrCheckActLt;
    private Date interPortAssignDate;
    private String interAssignDesId;
    private Date interPortDispatchDate;
    private String interTransRouteId;
    private Long interMeanTransId;
    private Long interLogId;
    private Long interTructId;
    private Long interTransCost;
    private Date interDesArrivalDate;
    private String interDesArrivalTime;
    private Date swapAssignDate;
    private String swapAssignDesId;
    private String swapRouteId;
    private String swapReason;
    private Long swapLogId;
    private Long swapTructTypeId;
    private Long swapTructId;
    private Long swapTransportCost;
    private Date swapDesArrivalDate;
    private Date orderMonth;
    private Date productionDate;
    private Date dlrDispatchPlan;
    private Long dlrLogId;
    private Long dlrTruckTypeId;
    private Long dlrTruckId;
    private String dlrTransRouteId;
    private Long stockDay;
    private Long stockFeeExport;
    private Long interiorColId;
    private Long vehicleStatus;
    private Date estimateAllocationMonth;
    private String remark;
    private Date customTmvArrDate;
    private Date interPortDispatchPlan;
    private Long interPortDispatchDelay;
    private String interPortDelayReason;
    private Long interDesArrivalDelay;
    private Date interDesArrivalPlan;
    private String interDesArrivalDelayReason;
    private Date swappingDispatchPlan;
    private Date swappingDispatchDate;
    private Date swappingDesArrivalPlan;
    private Long swappingArrivalDelay;
    private String swappingArrivalDelayReason;
    private Date dlrAssPlanDate;
    private Date dlrAssActualDate;
    private String pdiOwnerManual;
    private Date damageFoundDate;
    private String mlDelayDispatchReason;
    private Long mlDelayDispatch;
    private String pdiTransportRoute;
    private Date damageRepairDate;
    private Date dlrDispatchDate;
    private String dlrDispatchTime;
    private String blNo;
    private String customSheetNo;
    private Date priceConsultingDate;
    private Date tmvOcSigningDate;
    private Date customOcSigningDate;
    private String customRemark;
    private Date invoiceRequestDate;
    private String dlrDelay;
    private Date vrCertificateReceiveDate;
    private Date cbuProductionMonth;
    private Date originalLoPlanDate;
    private String loDelayReason;
    private Date dlrActualArrivalDate;
    private Date originalArrivalPlan;
    private String delayReasonOriArrPlan;
    private String docSendingVendor;
    private String swapRemark;
    private Long yardAreaId;
    private Long yardLocId;
    private String originalLoPlanTime;
    private Date tempLicenseDate;
    private Long diesel;
    private String transportWay;
    private String portTransportWay;
    private String dlrTransportWay;
    private String dlrDispatchPlanTime;
    private String sideAirbagLh;
    private String sideAirbagRh;
    private String kneeAirbag;
    private Long seqLexus;
    private Long selfDrivingTripRequest;
    private Date modifiedDateDispatchPlan;
    private Long dlrRqColId;
    private Date dlrColDeadline;
    private Timestamp tfsCreateDate;
    private String tmssNo;
    private Date latestLoPlanDate;
    private Date modifyDateAndon;
    private String engCode;
    private Long dlrAtId;
    private String payStatus;
    private Date modifyDateYardSystem;
    private String truckLocation;
    private String standbyLocationPdi;
    private Date mlMovingYardDate;
    private Long mlMovingYardTime;
    private String driverName;
    private Date modifiedDateRqColId;
    private Date modifyDateColDeadline;
    private Long pdiTime;
    private String pBeginInstallTime;
    private String pioBay;
    private String pioMember;
    private Date dispatchChangeReqDate;
    private Date dispatchChangeSendDate;
    private Date advanceRequestDate;
    private String dispatchChangeStatus;
    private Date newDispatchPlanDate;
    private Date dispatchChangeConfirmDate;
    private Date cbuDocDelivery;
    private String printInvDescAndon;
    private String pFinishInstallTime;

    @Id
    @Column(name = "ID", nullable = false)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "DLR_ID", nullable = true)
    public Long getDlrId() {
        return dlrId;
    }

    public void setDlrId(Long dlrId) {
        this.dlrId = dlrId;
    }

    @Basic
    @Column(name = "FRAME_NO", nullable = true, length = 50)
    public String getFrameNo() {
        return frameNo;
    }

    public void setFrameNo(String frameNo) {
        this.frameNo = frameNo;
    }

    @Basic
    @Column(name = "VIN", nullable = true, length = 100)
    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    @Basic
    @Column(name = "ENGINE_NO", nullable = true, length = 50)
    public String getEngineNo() {
        return engineNo;
    }

    public void setEngineNo(String engineNo) {
        this.engineNo = engineNo;
    }

    @Basic
    @Column(name = "ACC_ID", nullable = true)
    public Long getAccId() {
        return accId;
    }

    public void setAccId(Long accId) {
        this.accId = accId;
    }

    @Basic
    @Column(name = "CFU_ID", nullable = true)
    public Long getCfuId() {
        return cfuId;
    }

    public void setCfuId(Long cfuId) {
        this.cfuId = cfuId;
    }

    @Basic
    @Column(name = "GRA_ID", nullable = true)
    public Long getGraId() {
        return graId;
    }

    public void setGraId(Long graId) {
        this.graId = graId;
    }

    @Basic
    @Column(name = "LOC_ID", nullable = true)
    public Long getLocId() {
        return locId;
    }

    public void setLocId(Long locId) {
        this.locId = locId;
    }

    @Basic
    @Column(name = "TRU_ID", nullable = true)
    public Long getTruId() {
        return truId;
    }

    public void setTruId(Long truId) {
        this.truId = truId;
    }

    @Basic
    @Column(name = "INSTALL_DATE", nullable = true)
    public Date getInstallDate() {
        return installDate;
    }

    public void setInstallDate(Date installDate) {
        this.installDate = installDate;
    }

    @Basic
    @Column(name = "PAIN_IN_DATE", nullable = true)
    public Date getPainInDate() {
        return painInDate;
    }

    public void setPainInDate(Date painInDate) {
        this.painInDate = painInDate;
    }

    @Basic
    @Column(name = "CCR_LOT_NO", nullable = true, length = 20)
    public String getCcrLotNo() {
        return ccrLotNo;
    }

    public void setCcrLotNo(String ccrLotNo) {
        this.ccrLotNo = ccrLotNo;
    }

    @Basic
    @Column(name = "CCR_NO_IN_LOT", nullable = true, length = 20)
    public String getCcrNoInLot() {
        return ccrNoInLot;
    }

    public void setCcrNoInLot(String ccrNoInLot) {
        this.ccrNoInLot = ccrNoInLot;
    }

    @Basic
    @Column(name = "CCR_BODY_NO", nullable = true, length = 20)
    public String getCcrBodyNo() {
        return ccrBodyNo;
    }

    public void setCcrBodyNo(String ccrBodyNo) {
        this.ccrBodyNo = ccrBodyNo;
    }

    @Basic
    @Column(name = "CCR_A_IN", nullable = true, length = 20)
    public String getCcrAIn() {
        return ccrAIn;
    }

    public void setCcrAIn(String ccrAIn) {
        this.ccrAIn = ccrAIn;
    }

    @Basic
    @Column(name = "CCR_A_OUT", nullable = true, length = 20)
    public String getCcrAOut() {
        return ccrAOut;
    }

    public void setCcrAOut(String ccrAOut) {
        this.ccrAOut = ccrAOut;
    }

    @Basic
    @Column(name = "KEY_CODE", nullable = true, length = 50)
    public String getKeyCode() {
        return keyCode;
    }

    public void setKeyCode(String keyCode) {
        this.keyCode = keyCode;
    }

    @Basic
    @Column(name = "QC_DRIVER_NO", nullable = true, length = 20)
    public String getQcDriverNo() {
        return qcDriverNo;
    }

    public void setQcDriverNo(String qcDriverNo) {
        this.qcDriverNo = qcDriverNo;
    }

    @Basic
    @Column(name = "QC_PASSENGER_NO", nullable = true, length = 20)
    public String getQcPassengerNo() {
        return qcPassengerNo;
    }

    public void setQcPassengerNo(String qcPassengerNo) {
        this.qcPassengerNo = qcPassengerNo;
    }

    @Basic
    @Column(name = "QC_ENGNO", nullable = true, length = 20)
    public String getQcEngno() {
        return qcEngno;
    }

    public void setQcEngno(String qcEngno) {
        this.qcEngno = qcEngno;
    }

    @Basic
    @Column(name = "QC_TRANMISSION_NO", nullable = true, length = 20)
    public String getQcTranmissionNo() {
        return qcTranmissionNo;
    }

    public void setQcTranmissionNo(String qcTranmissionNo) {
        this.qcTranmissionNo = qcTranmissionNo;
    }

    @Basic
    @Column(name = "QC_AXLE_NO", nullable = true, length = 20)
    public String getQcAxleNo() {
        return qcAxleNo;
    }

    public void setQcAxleNo(String qcAxleNo) {
        this.qcAxleNo = qcAxleNo;
    }

    @Basic
    @Column(name = "QC_LOSHIFT", nullable = true, length = 1)
    public String getQcLoshift() {
        return qcLoshift;
    }

    public void setQcLoshift(String qcLoshift) {
        this.qcLoshift = qcLoshift;
    }

    @Basic
    @Column(name = "LINE_OFF_DATE", nullable = true)
    public Date getLineOffDate() {
        return lineOffDate;
    }

    public void setLineOffDate(Date lineOffDate) {
        this.lineOffDate = lineOffDate;
    }

    @Basic
    @Column(name = "ASS_CHANGE_FROM", nullable = true)
    public Long getAssChangeFrom() {
        return assChangeFrom;
    }

    public void setAssChangeFrom(Long assChangeFrom) {
        this.assChangeFrom = assChangeFrom;
    }

    @Basic
    @Column(name = "ASS_CHANGE_REASON", nullable = true, length = 255)
    public String getAssChangeReason() {
        return assChangeReason;
    }

    public void setAssChangeReason(String assChangeReason) {
        this.assChangeReason = assChangeReason;
    }

    @Basic
    @Column(name = "ASS_ALLO_MONTH", nullable = true)
    public Date getAssAlloMonth() {
        return assAlloMonth;
    }

    public void setAssAlloMonth(Date assAlloMonth) {
        this.assAlloMonth = assAlloMonth;
    }

    @Basic
    @Column(name = "DLR_PAYMENT_PLAN", nullable = true)
    public Date getDlrPaymentPlan() {
        return dlrPaymentPlan;
    }

    public void setDlrPaymentPlan(Date dlrPaymentPlan) {
        this.dlrPaymentPlan = dlrPaymentPlan;
    }

    @Basic
    @Column(name = "R_F_PAYMENT_DATE", nullable = true)
    public Date getrFPaymentDate() {
        return rFPaymentDate;
    }

    public void setrFPaymentDate(Date rFPaymentDate) {
        this.rFPaymentDate = rFPaymentDate;
    }

    @Basic
    @Column(name = "PAY_INVOICE_NO", nullable = true, length = 50)
    public String getPayInvoiceNo() {
        return payInvoiceNo;
    }

    public void setPayInvoiceNo(String payInvoiceNo) {
        this.payInvoiceNo = payInvoiceNo;
    }

    @Basic
    @Column(name = "PAY_ACTUAL_INVOICE_DATE", nullable = true)
    public Date getPayActualInvoiceDate() {
        return payActualInvoiceDate;
    }

    public void setPayActualInvoiceDate(Date payActualInvoiceDate) {
        this.payActualInvoiceDate = payActualInvoiceDate;
    }

    @Basic
    @Column(name = "PAY_VN_AMOUNT", nullable = true)
    public Long getPayVnAmount() {
        return payVnAmount;
    }

    public void setPayVnAmount(Long payVnAmount) {
        this.payVnAmount = payVnAmount;
    }

    @Basic
    @Column(name = "PAY_USD_AMOUNT", nullable = true)
    public Long getPayUsdAmount() {
        return payUsdAmount;
    }

    public void setPayUsdAmount(Long payUsdAmount) {
        this.payUsdAmount = payUsdAmount;
    }

    @Basic
    @Column(name = "PAY_HOLDBACK_AMOUNT", nullable = true)
    public Long getPayHoldbackAmount() {
        return payHoldbackAmount;
    }

    public void setPayHoldbackAmount(Long payHoldbackAmount) {
        this.payHoldbackAmount = payHoldbackAmount;
    }

    @Basic
    @Column(name = "PAY_R_F_PAYBACK", nullable = true)
    public Long getPayRFPayback() {
        return payRFPayback;
    }

    public void setPayRFPayback(Long payRFPayback) {
        this.payRFPayback = payRFPayback;
    }

    @Basic
    @Column(name = "PAY_DISCOUNT_AMOUNT", nullable = true)
    public Long getPayDiscountAmount() {
        return payDiscountAmount;
    }

    public void setPayDiscountAmount(Long payDiscountAmount) {
        this.payDiscountAmount = payDiscountAmount;
    }

    @Basic
    @Column(name = "DOCUMENT_TMV_SITUATION_ID", nullable = true, length = 255)
    public String getDocumentTmvSituationId() {
        return documentTmvSituationId;
    }

    public void setDocumentTmvSituationId(String documentTmvSituationId) {
        this.documentTmvSituationId = documentTmvSituationId;
    }

    @Basic
    @Column(name = "DOCUMENT_DELIVERY_DATE", nullable = true)
    public Date getDocumentDeliveryDate() {
        return documentDeliveryDate;
    }

    public void setDocumentDeliveryDate(Date documentDeliveryDate) {
        this.documentDeliveryDate = documentDeliveryDate;
    }

    @Basic
    @Column(name = "DOCUMENT_DELIVERY_TIME", nullable = true, length = 10)
    public String getDocumentDeliveryTime() {
        return documentDeliveryTime;
    }

    public void setDocumentDeliveryTime(String documentDeliveryTime) {
        this.documentDeliveryTime = documentDeliveryTime;
    }

    @Basic
    @Column(name = "DOCUMENT_ARRIVAL_DATE", nullable = true)
    public Date getDocumentArrivalDate() {
        return documentArrivalDate;
    }

    public void setDocumentArrivalDate(Date documentArrivalDate) {
        this.documentArrivalDate = documentArrivalDate;
    }

    @Basic
    @Column(name = "DOCUMENT_ARRIVAL_TIME", nullable = true, length = 10)
    public String getDocumentArrivalTime() {
        return documentArrivalTime;
    }

    public void setDocumentArrivalTime(String documentArrivalTime) {
        this.documentArrivalTime = documentArrivalTime;
    }

    @Basic
    @Column(name = "DOCUMENT_ARRIVAL_REMARK", nullable = true, length = 255)
    public String getDocumentArrivalRemark() {
        return documentArrivalRemark;
    }

    public void setDocumentArrivalRemark(String documentArrivalRemark) {
        this.documentArrivalRemark = documentArrivalRemark;
    }

    @Basic
    @Column(name = "DOCUMENT_DLR_SITUATION_ID", nullable = true, length = 255)
    public String getDocumentDlrSituationId() {
        return documentDlrSituationId;
    }

    public void setDocumentDlrSituationId(String documentDlrSituationId) {
        this.documentDlrSituationId = documentDlrSituationId;
    }

    @Basic
    @Column(name = "FLEET_PRICE", nullable = true)
    public Long getFleetPrice() {
        return fleetPrice;
    }

    public void setFleetPrice(Long fleetPrice) {
        this.fleetPrice = fleetPrice;
    }

    @Basic
    @Column(name = "ML_DELIVERY_DATE", nullable = true)
    public Date getMlDeliveryDate() {
        return mlDeliveryDate;
    }

    public void setMlDeliveryDate(Date mlDeliveryDate) {
        this.mlDeliveryDate = mlDeliveryDate;
    }

    @Basic
    @Column(name = "ML_DELIVERY_TIME", nullable = true, length = 10)
    public String getMlDeliveryTime() {
        return mlDeliveryTime;
    }

    public void setMlDeliveryTime(String mlDeliveryTime) {
        this.mlDeliveryTime = mlDeliveryTime;
    }

    @Basic
    @Column(name = "ML_YARD_AREA_ID", nullable = true)
    public Long getMlYardAreaId() {
        return mlYardAreaId;
    }

    public void setMlYardAreaId(Long mlYardAreaId) {
        this.mlYardAreaId = mlYardAreaId;
    }

    @Basic
    @Column(name = "ML_YARD_LOC_ID", nullable = true)
    public Long getMlYardLocId() {
        return mlYardLocId;
    }

    public void setMlYardLocId(Long mlYardLocId) {
        this.mlYardLocId = mlYardLocId;
    }

    @Basic
    @Column(name = "ML_LICIENCE", nullable = true, length = 20)
    public String getMlLicience() {
        return mlLicience;
    }

    public void setMlLicience(String mlLicience) {
        this.mlLicience = mlLicience;
    }

    @Basic
    @Column(name = "ML_LOG_ID", nullable = true)
    public Long getMlLogId() {
        return mlLogId;
    }

    public void setMlLogId(Long mlLogId) {
        this.mlLogId = mlLogId;
    }

    @Basic
    @Column(name = "ML_TRUCK_TYPE_ID", nullable = true)
    public Long getMlTruckTypeId() {
        return mlTruckTypeId;
    }

    public void setMlTruckTypeId(Long mlTruckTypeId) {
        this.mlTruckTypeId = mlTruckTypeId;
    }

    @Basic
    @Column(name = "ML_TRUCK_ID", nullable = true)
    public Long getMlTruckId() {
        return mlTruckId;
    }

    public void setMlTruckId(Long mlTruckId) {
        this.mlTruckId = mlTruckId;
    }

    @Basic
    @Column(name = "ML_OTHER_DLR", nullable = true)
    public Long getMlOtherDlr() {
        return mlOtherDlr;
    }

    public void setMlOtherDlr(Long mlOtherDlr) {
        this.mlOtherDlr = mlOtherDlr;
    }

    @Basic
    @Column(name = "PDI_PLAN_ARRIVAL_DATE", nullable = true)
    public Date getPdiPlanArrivalDate() {
        return pdiPlanArrivalDate;
    }

    public void setPdiPlanArrivalDate(Date pdiPlanArrivalDate) {
        this.pdiPlanArrivalDate = pdiPlanArrivalDate;
    }

    @Basic
    @Column(name = "PDI_PLAN_ARRIVAL_TIME", nullable = true, length = 10)
    public String getPdiPlanArrivalTime() {
        return pdiPlanArrivalTime;
    }

    public void setPdiPlanArrivalTime(String pdiPlanArrivalTime) {
        this.pdiPlanArrivalTime = pdiPlanArrivalTime;
    }

    @Basic
    @Column(name = "PDI_ARRIVAL_DATE", nullable = true)
    public Date getPdiArrivalDate() {
        return pdiArrivalDate;
    }

    public void setPdiArrivalDate(Date pdiArrivalDate) {
        this.pdiArrivalDate = pdiArrivalDate;
    }

    @Basic
    @Column(name = "PDI_ARRIVAL_TIME", nullable = true, length = 10)
    public String getPdiArrivalTime() {
        return pdiArrivalTime;
    }

    public void setPdiArrivalTime(String pdiArrivalTime) {
        this.pdiArrivalTime = pdiArrivalTime;
    }

    @Basic
    @Column(name = "PDI_DELAY_REASON", nullable = true, length = 255)
    public String getPdiDelayReason() {
        return pdiDelayReason;
    }

    public void setPdiDelayReason(String pdiDelayReason) {
        this.pdiDelayReason = pdiDelayReason;
    }

    @Basic
    @Column(name = "PDI_VEHICLE_SITUATION_ID", nullable = true, length = 255)
    public String getPdiVehicleSituationId() {
        return pdiVehicleSituationId;
    }

    public void setPdiVehicleSituationId(String pdiVehicleSituationId) {
        this.pdiVehicleSituationId = pdiVehicleSituationId;
    }

    @Basic
    @Column(name = "PDI_YARD_AREA", nullable = true)
    public Long getPdiYardArea() {
        return pdiYardArea;
    }

    public void setPdiYardArea(Long pdiYardArea) {
        this.pdiYardArea = pdiYardArea;
    }

    @Basic
    @Column(name = "PDI_LOC_ID", nullable = true)
    public Long getPdiLocId() {
        return pdiLocId;
    }

    public void setPdiLocId(Long pdiLocId) {
        this.pdiLocId = pdiLocId;
    }

    @Basic
    @Column(name = "PDI_DELIVERY_DATE", nullable = true)
    public Date getPdiDeliveryDate() {
        return pdiDeliveryDate;
    }

    public void setPdiDeliveryDate(Date pdiDeliveryDate) {
        this.pdiDeliveryDate = pdiDeliveryDate;
    }

    @Basic
    @Column(name = "PDI_DELIVERY_TIME", nullable = true, length = 10)
    public String getPdiDeliveryTime() {
        return pdiDeliveryTime;
    }

    public void setPdiDeliveryTime(String pdiDeliveryTime) {
        this.pdiDeliveryTime = pdiDeliveryTime;
    }

    @Basic
    @Column(name = "PDI_LOG_ID", nullable = true)
    public Long getPdiLogId() {
        return pdiLogId;
    }

    public void setPdiLogId(Long pdiLogId) {
        this.pdiLogId = pdiLogId;
    }

    @Basic
    @Column(name = "PDI_TRUCT_TYPE_ID", nullable = true)
    public Long getPdiTructTypeId() {
        return pdiTructTypeId;
    }

    public void setPdiTructTypeId(Long pdiTructTypeId) {
        this.pdiTructTypeId = pdiTructTypeId;
    }

    @Basic
    @Column(name = "PDI_TRUCT_ID", nullable = true)
    public Long getPdiTructId() {
        return pdiTructId;
    }

    public void setPdiTructId(Long pdiTructId) {
        this.pdiTructId = pdiTructId;
    }

    @Basic
    @Column(name = "PDI_TRANS_COST", nullable = true)
    public Long getPdiTransCost() {
        return pdiTransCost;
    }

    public void setPdiTransCost(Long pdiTransCost) {
        this.pdiTransCost = pdiTransCost;
    }

    @Basic
    @Column(name = "PDI_STOCK_MAINTENANCE_DATE", nullable = true)
    public Date getPdiStockMaintenanceDate() {
        return pdiStockMaintenanceDate;
    }

    public void setPdiStockMaintenanceDate(Date pdiStockMaintenanceDate) {
        this.pdiStockMaintenanceDate = pdiStockMaintenanceDate;
    }

    @Basic
    @Column(name = "ML_ARRIVAL_PLAN_DATE", nullable = true)
    public Date getMlArrivalPlanDate() {
        return mlArrivalPlanDate;
    }

    public void setMlArrivalPlanDate(Date mlArrivalPlanDate) {
        this.mlArrivalPlanDate = mlArrivalPlanDate;
    }

    @Basic
    @Column(name = "ML_ARRIVAL_PLAN_TIME", nullable = true, length = 10)
    public String getMlArrivalPlanTime() {
        return mlArrivalPlanTime;
    }

    public void setMlArrivalPlanTime(String mlArrivalPlanTime) {
        this.mlArrivalPlanTime = mlArrivalPlanTime;
    }

    @Basic
    @Column(name = "ML_ARRIVAL_ACTUAL_DATE", nullable = true)
    public Date getMlArrivalActualDate() {
        return mlArrivalActualDate;
    }

    public void setMlArrivalActualDate(Date mlArrivalActualDate) {
        this.mlArrivalActualDate = mlArrivalActualDate;
    }

    @Basic
    @Column(name = "ML_ARRIVAL_ACTUAL_TIME", nullable = true, length = 10)
    public String getMlArrivalActualTime() {
        return mlArrivalActualTime;
    }

    public void setMlArrivalActualTime(String mlArrivalActualTime) {
        this.mlArrivalActualTime = mlArrivalActualTime;
    }

    @Basic
    @Column(name = "ML_ARRIVAL_DELAY_REASON", nullable = true, length = 255)
    public String getMlArrivalDelayReason() {
        return mlArrivalDelayReason;
    }

    public void setMlArrivalDelayReason(String mlArrivalDelayReason) {
        this.mlArrivalDelayReason = mlArrivalDelayReason;
    }

    @Basic
    @Column(name = "ML_ARRIVAL_SITUATION_ID", nullable = true, length = 255)
    public String getMlArrivalSituationId() {
        return mlArrivalSituationId;
    }

    public void setMlArrivalSituationId(String mlArrivalSituationId) {
        this.mlArrivalSituationId = mlArrivalSituationId;
    }

    @Basic
    @Column(name = "ML_ARRIVAL_TRANS_COST", nullable = true)
    public Long getMlArrivalTransCost() {
        return mlArrivalTransCost;
    }

    public void setMlArrivalTransCost(Long mlArrivalTransCost) {
        this.mlArrivalTransCost = mlArrivalTransCost;
    }

    @Basic
    @Column(name = "DLR_PLAN_ARRIVAL_DATE", nullable = true)
    public Date getDlrPlanArrivalDate() {
        return dlrPlanArrivalDate;
    }

    public void setDlrPlanArrivalDate(Date dlrPlanArrivalDate) {
        this.dlrPlanArrivalDate = dlrPlanArrivalDate;
    }

    @Basic
    @Column(name = "DLR_PLAN_ARRIVAL_TIME", nullable = true, length = 10)
    public String getDlrPlanArrivalTime() {
        return dlrPlanArrivalTime;
    }

    public void setDlrPlanArrivalTime(String dlrPlanArrivalTime) {
        this.dlrPlanArrivalTime = dlrPlanArrivalTime;
    }

    @Basic
    @Column(name = "DLR_ARRIVAL_DATE", nullable = true)
    public Date getDlrArrivalDate() {
        return dlrArrivalDate;
    }

    public void setDlrArrivalDate(Date dlrArrivalDate) {
        this.dlrArrivalDate = dlrArrivalDate;
    }

    @Basic
    @Column(name = "DLR_ARRIVAL_TIME", nullable = true, length = 10)
    public String getDlrArrivalTime() {
        return dlrArrivalTime;
    }

    public void setDlrArrivalTime(String dlrArrivalTime) {
        this.dlrArrivalTime = dlrArrivalTime;
    }

    @Basic
    @Column(name = "DLR_DELAY_REASON", nullable = true, length = 255)
    public String getDlrDelayReason() {
        return dlrDelayReason;
    }

    public void setDlrDelayReason(String dlrDelayReason) {
        this.dlrDelayReason = dlrDelayReason;
    }

    @Basic
    @Column(name = "DLR_TRANS_COST", nullable = true)
    public Long getDlrTransCost() {
        return dlrTransCost;
    }

    public void setDlrTransCost(Long dlrTransCost) {
        this.dlrTransCost = dlrTransCost;
    }

    @Basic
    @Column(name = "DLR_HAS_FLOOR_MAT", nullable = true, length = 5)
    public String getDlrHasFloorMat() {
        return dlrHasFloorMat;
    }

    public void setDlrHasFloorMat(String dlrHasFloorMat) {
        this.dlrHasFloorMat = dlrHasFloorMat;
    }

    @Basic
    @Column(name = "DLR_GASOLINE", nullable = true)
    public Long getDlrGasoline() {
        return dlrGasoline;
    }

    public void setDlrGasoline(Long dlrGasoline) {
        this.dlrGasoline = dlrGasoline;
    }

    @Basic
    @Column(name = "DLR_YARD_AREA_ID", nullable = true)
    public Long getDlrYardAreaId() {
        return dlrYardAreaId;
    }

    public void setDlrYardAreaId(Long dlrYardAreaId) {
        this.dlrYardAreaId = dlrYardAreaId;
    }

    @Basic
    @Column(name = "DLR_YARD_LOC_ID", nullable = true)
    public Long getDlrYardLocId() {
        return dlrYardLocId;
    }

    public void setDlrYardLocId(Long dlrYardLocId) {
        this.dlrYardLocId = dlrYardLocId;
    }

    @Basic
    @Column(name = "DAMAGE_IS", nullable = true, length = 1)
    public String getDamageIs() {
        return damageIs;
    }

    public void setDamageIs(String damageIs) {
        this.damageIs = damageIs;
    }

    @Basic
    @Column(name = "DAMAGE_TYPE_ID", nullable = true)
    public Long getDamageTypeId() {
        return damageTypeId;
    }

    public void setDamageTypeId(Long damageTypeId) {
        this.damageTypeId = damageTypeId;
    }

    @Basic
    @Column(name = "DAMAGE_REPAIRE_COST", nullable = true)
    public Long getDamageRepaireCost() {
        return damageRepaireCost;
    }

    public void setDamageRepaireCost(Long damageRepaireCost) {
        this.damageRepaireCost = damageRepaireCost;
    }

    @Basic
    @Column(name = "DAMAGE_PLACE", nullable = true, length = 255)
    public String getDamagePlace() {
        return damagePlace;
    }

    public void setDamagePlace(String damagePlace) {
        this.damagePlace = damagePlace;
    }

    @Basic
    @Column(name = "DAMAGE_REASON", nullable = true, length = 255)
    public String getDamageReason() {
        return damageReason;
    }

    public void setDamageReason(String damageReason) {
        this.damageReason = damageReason;
    }

    @Basic
    @Column(name = "DAMAGE_DES", nullable = true, length = 255)
    public String getDamageDes() {
        return damageDes;
    }

    public void setDamageDes(String damageDes) {
        this.damageDes = damageDes;
    }

    @Basic
    @Column(name = "DAMAGE_INS_COMPANY_ID", nullable = true)
    public Long getDamageInsCompanyId() {
        return damageInsCompanyId;
    }

    public void setDamageInsCompanyId(Long damageInsCompanyId) {
        this.damageInsCompanyId = damageInsCompanyId;
    }

    @Basic
    @Column(name = "DAMAGE_CLAIM_DATE", nullable = true)
    public Date getDamageClaimDate() {
        return damageClaimDate;
    }

    public void setDamageClaimDate(Date damageClaimDate) {
        this.damageClaimDate = damageClaimDate;
    }

    @Basic
    @Column(name = "DAMAGE_FINISHING_DATE", nullable = true)
    public Date getDamageFinishingDate() {
        return damageFinishingDate;
    }

    public void setDamageFinishingDate(Date damageFinishingDate) {
        this.damageFinishingDate = damageFinishingDate;
    }

    @Basic
    @Column(name = "STOCK_SERVICE_MAINTENANCE_DATE", nullable = true)
    public Date getStockServiceMaintenanceDate() {
        return stockServiceMaintenanceDate;
    }

    public void setStockServiceMaintenanceDate(Date stockServiceMaintenanceDate) {
        this.stockServiceMaintenanceDate = stockServiceMaintenanceDate;
    }

    @Basic
    @Column(name = "STOCK_REMARK", nullable = true, length = 255)
    public String getStockRemark() {
        return stockRemark;
    }

    public void setStockRemark(String stockRemark) {
        this.stockRemark = stockRemark;
    }

    @Basic
    @Column(name = "CREATED_BY", nullable = true)
    public Long getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Long createdBy) {
        this.createdBy = createdBy;
    }

    @Basic
    @Column(name = "CREATE_DATE", nullable = true)
    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    @Basic
    @Column(name = "MODIFIED_BY", nullable = true)
    public Long getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(Long modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    @Basic
    @Column(name = "MODIFY_DATE", nullable = true)
    public Date getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(Date modifyDate) {
        this.modifyDate = modifyDate;
    }

    @Basic
    @Column(name = "DLR_RECEIVE", nullable = true, length = 250)
    public String getDlrReceive() {
        return dlrReceive;
    }

    public void setDlrReceive(String dlrReceive) {
        this.dlrReceive = dlrReceive;
    }

    @Basic
    @Column(name = "DLR_REMARK", nullable = true, length = 250)
    public String getDlrRemark() {
        return dlrRemark;
    }

    public void setDlrRemark(String dlrRemark) {
        this.dlrRemark = dlrRemark;
    }

    @Basic
    @Column(name = "DLR_STATUS", nullable = true, length = 1)
    public String getDlrStatus() {
        return dlrStatus;
    }

    public void setDlrStatus(String dlrStatus) {
        this.dlrStatus = dlrStatus;
    }

    @Basic
    @Column(name = "ML_PLAN_DELIVERY_DATE", nullable = true)
    public Date getMlPlanDeliveryDate() {
        return mlPlanDeliveryDate;
    }

    public void setMlPlanDeliveryDate(Date mlPlanDeliveryDate) {
        this.mlPlanDeliveryDate = mlPlanDeliveryDate;
    }

    @Basic
    @Column(name = "ML_PLAN_DELIVERY_TIME", nullable = true, length = 12)
    public String getMlPlanDeliveryTime() {
        return mlPlanDeliveryTime;
    }

    public void setMlPlanDeliveryTime(String mlPlanDeliveryTime) {
        this.mlPlanDeliveryTime = mlPlanDeliveryTime;
    }

    @Basic
    @Column(name = "PDI_PLAN_DELIVERY_TIME", nullable = true, length = 12)
    public String getPdiPlanDeliveryTime() {
        return pdiPlanDeliveryTime;
    }

    public void setPdiPlanDeliveryTime(String pdiPlanDeliveryTime) {
        this.pdiPlanDeliveryTime = pdiPlanDeliveryTime;
    }

    @Basic
    @Column(name = "PDI_PLAN_DELIVERY_DATE", nullable = true)
    public Date getPdiPlanDeliveryDate() {
        return pdiPlanDeliveryDate;
    }

    public void setPdiPlanDeliveryDate(Date pdiPlanDeliveryDate) {
        this.pdiPlanDeliveryDate = pdiPlanDeliveryDate;
    }

    @Basic
    @Column(name = "DLR_VEHICLE_SITUATION_ID", nullable = true, length = 255)
    public String getDlrVehicleSituationId() {
        return dlrVehicleSituationId;
    }

    public void setDlrVehicleSituationId(String dlrVehicleSituationId) {
        this.dlrVehicleSituationId = dlrVehicleSituationId;
    }

    @Basic
    @Column(name = "ASSIGNMENT_DATE", nullable = true)
    public Date getAssignmentDate() {
        return assignmentDate;
    }

    public void setAssignmentDate(Date assignmentDate) {
        this.assignmentDate = assignmentDate;
    }

    @Basic
    @Column(name = "PAYMENT_BY", nullable = true, length = 50)
    public String getPaymentBy() {
        return paymentBy;
    }

    public void setPaymentBy(String paymentBy) {
        this.paymentBy = paymentBy;
    }

    @Basic
    @Column(name = "GRA_PRO_ID", nullable = true)
    public Long getGraProId() {
        return graProId;
    }

    public void setGraProId(Long graProId) {
        this.graProId = graProId;
    }

    @Basic
    @Column(name = "TRANSPORT_ROUTE", nullable = true, length = 50)
    public String getTransportRoute() {
        return transportRoute;
    }

    public void setTransportRoute(String transportRoute) {
        this.transportRoute = transportRoute;
    }

    @Basic
    @Column(name = "IS_EXPORT", nullable = true, length = 1)
    public String getIsExport() {
        return isExport;
    }

    public void setIsExport(String isExport) {
        this.isExport = isExport;
    }

    @Basic
    @Column(name = "SEQ_NO", nullable = true, length = 50)
    public String getSeqNo() {
        return seqNo;
    }

    public void setSeqNo(String seqNo) {
        this.seqNo = seqNo;
    }

    @Basic
    @Column(name = "PDI_CONDUCTING", nullable = true)
    public Date getPdiConducting() {
        return pdiConducting;
    }

    public void setPdiConducting(Date pdiConducting) {
        this.pdiConducting = pdiConducting;
    }

    @Basic
    @Column(name = "FAP_ID", nullable = true)
    public Long getFapId() {
        return fapId;
    }

    public void setFapId(Long fapId) {
        this.fapId = fapId;
    }

    @Basic
    @Column(name = "FLEET_CUSTOMER", nullable = true, length = 255)
    public String getFleetCustomer() {
        return fleetCustomer;
    }

    public void setFleetCustomer(String fleetCustomer) {
        this.fleetCustomer = fleetCustomer;
    }

    @Basic
    @Column(name = "VR_DATE", nullable = true)
    public Date getVrDate() {
        return vrDate;
    }

    public void setVrDate(Date vrDate) {
        this.vrDate = vrDate;
    }

    @Basic
    @Column(name = "CBU_DOCUMENT", nullable = true)
    public Date getCbuDocument() {
        return cbuDocument;
    }

    public void setCbuDocument(Date cbuDocument) {
        this.cbuDocument = cbuDocument;
    }

    @Basic
    @Column(name = "IS_REPORT", nullable = true, length = 1)
    public String getIsReport() {
        return isReport;
    }

    public void setIsReport(String isReport) {
        this.isReport = isReport;
    }

    @Basic
    @Column(name = "TFS_PROCESS", nullable = true, length = 1)
    public String getTfsProcess() {
        return tfsProcess;
    }

    public void setTfsProcess(String tfsProcess) {
        this.tfsProcess = tfsProcess;
    }

    @Basic
    @Column(name = "SALES_DLR_ID", nullable = true)
    public Long getSalesDlrId() {
        return salesDlrId;
    }

    public void setSalesDlrId(Long salesDlrId) {
        this.salesDlrId = salesDlrId;
    }

    @Basic
    @Column(name = "SALES_DATE", nullable = true)
    public Date getSalesDate() {
        return salesDate;
    }

    public void setSalesDate(Date salesDate) {
        this.salesDate = salesDate;
    }

    @Basic
    @Column(name = "SALES_STATUS", nullable = true, length = 50)
    public String getSalesStatus() {
        return salesStatus;
    }

    public void setSalesStatus(String salesStatus) {
        this.salesStatus = salesStatus;
    }

    @Basic
    @Column(name = "TFS_AMOUNT", nullable = true)
    public Long getTfsAmount() {
        return tfsAmount;
    }

    public void setTfsAmount(Long tfsAmount) {
        this.tfsAmount = tfsAmount;
    }

    @Basic
    @Column(name = "P_INSTALL_DATE", nullable = true)
    public Date getpInstallDate() {
        return pInstallDate;
    }

    public void setpInstallDate(Date pInstallDate) {
        this.pInstallDate = pInstallDate;
    }

    @Basic
    @Column(name = "P_INSTALL_TIME", nullable = true, length = 12)
    public String getpInstallTime() {
        return pInstallTime;
    }

    public void setpInstallTime(String pInstallTime) {
        this.pInstallTime = pInstallTime;
    }

    @Basic
    @Column(name = "INSTALL_TIME", nullable = true, length = 12)
    public String getInstallTime() {
        return installTime;
    }

    public void setInstallTime(String installTime) {
        this.installTime = installTime;
    }

    @Basic
    @Column(name = "DEFER_PAYMENT", nullable = true)
    public Date getDeferPayment() {
        return deferPayment;
    }

    public void setDeferPayment(Date deferPayment) {
        this.deferPayment = deferPayment;
    }

    @Basic
    @Column(name = "PDI_DATE", nullable = true)
    public Date getPdiDate() {
        return pdiDate;
    }

    public void setPdiDate(Date pdiDate) {
        this.pdiDate = pdiDate;
    }

    @Basic
    @Column(name = "CBU_LOCATION", nullable = true, length = 10)
    public String getCbuLocation() {
        return cbuLocation;
    }

    public void setCbuLocation(String cbuLocation) {
        this.cbuLocation = cbuLocation;
    }

    @Basic
    @Column(name = "CBU_DOC_PLAN", nullable = true)
    public Date getCbuDocPlan() {
        return cbuDocPlan;
    }

    public void setCbuDocPlan(Date cbuDocPlan) {
        this.cbuDocPlan = cbuDocPlan;
    }

    @Basic
    @Column(name = "ST_DELIVERY", nullable = true)
    public Date getStDelivery() {
        return stDelivery;
    }

    public void setStDelivery(Date stDelivery) {
        this.stDelivery = stDelivery;
    }

    @Basic
    @Column(name = "PIO_STATUS", nullable = true, length = 1)
    public String getPioStatus() {
        return pioStatus;
    }

    public void setPioStatus(String pioStatus) {
        this.pioStatus = pioStatus;
    }

    @Basic
    @Column(name = "FIRST_TIME_MAINT_PLAN", nullable = true)
    public Date getFirstTimeMaintPlan() {
        return firstTimeMaintPlan;
    }

    public void setFirstTimeMaintPlan(Date firstTimeMaintPlan) {
        this.firstTimeMaintPlan = firstTimeMaintPlan;
    }

    @Basic
    @Column(name = "FIRST_TIME_MAINT_ACT", nullable = true)
    public Date getFirstTimeMaintAct() {
        return firstTimeMaintAct;
    }

    public void setFirstTimeMaintAct(Date firstTimeMaintAct) {
        this.firstTimeMaintAct = firstTimeMaintAct;
    }

    @Basic
    @Column(name = "SECOND_TIME_MAINT_PLAN", nullable = true)
    public Date getSecondTimeMaintPlan() {
        return secondTimeMaintPlan;
    }

    public void setSecondTimeMaintPlan(Date secondTimeMaintPlan) {
        this.secondTimeMaintPlan = secondTimeMaintPlan;
    }

    @Basic
    @Column(name = "SECOND_TIME_MAINT_ACT", nullable = true)
    public Date getSecondTimeMaintAct() {
        return secondTimeMaintAct;
    }

    public void setSecondTimeMaintAct(Date secondTimeMaintAct) {
        this.secondTimeMaintAct = secondTimeMaintAct;
    }

    @Basic
    @Column(name = "THIRD_TIME_MAINT_PLAN", nullable = true)
    public Timestamp getThirdTimeMaintPlan() {
        return thirdTimeMaintPlan;
    }

    public void setThirdTimeMaintPlan(Timestamp thirdTimeMaintPlan) {
        this.thirdTimeMaintPlan = thirdTimeMaintPlan;
    }

    @Basic
    @Column(name = "THIRD_TIME_MAINT_ACT", nullable = true)
    public Date getThirdTimeMaintAct() {
        return thirdTimeMaintAct;
    }

    public void setThirdTimeMaintAct(Date thirdTimeMaintAct) {
        this.thirdTimeMaintAct = thirdTimeMaintAct;
    }

    @Basic
    @Column(name = "FORTH_TIME_MAINT_PLAN", nullable = true)
    public Date getForthTimeMaintPlan() {
        return forthTimeMaintPlan;
    }

    public void setForthTimeMaintPlan(Date forthTimeMaintPlan) {
        this.forthTimeMaintPlan = forthTimeMaintPlan;
    }

    @Basic
    @Column(name = "FORTH_TIME_MAINT_ACT", nullable = true)
    public Date getForthTimeMaintAct() {
        return forthTimeMaintAct;
    }

    public void setForthTimeMaintAct(Date forthTimeMaintAct) {
        this.forthTimeMaintAct = forthTimeMaintAct;
    }

    @Basic
    @Column(name = "FIFTH_TIME_MAINT_PLAN", nullable = true)
    public Date getFifthTimeMaintPlan() {
        return fifthTimeMaintPlan;
    }

    public void setFifthTimeMaintPlan(Date fifthTimeMaintPlan) {
        this.fifthTimeMaintPlan = fifthTimeMaintPlan;
    }

    @Basic
    @Column(name = "FIFTH_TIME_MAINT_ACT", nullable = true)
    public Date getFifthTimeMaintAct() {
        return fifthTimeMaintAct;
    }

    public void setFifthTimeMaintAct(Date fifthTimeMaintAct) {
        this.fifthTimeMaintAct = fifthTimeMaintAct;
    }

    @Basic
    @Column(name = "SIXTH_TIME_MAINT_PLAN", nullable = true)
    public Date getSixthTimeMaintPlan() {
        return sixthTimeMaintPlan;
    }

    public void setSixthTimeMaintPlan(Date sixthTimeMaintPlan) {
        this.sixthTimeMaintPlan = sixthTimeMaintPlan;
    }

    @Basic
    @Column(name = "SIXTH_TIME_MAINT_ACT", nullable = true)
    public Date getSixthTimeMaintAct() {
        return sixthTimeMaintAct;
    }

    public void setSixthTimeMaintAct(Date sixthTimeMaintAct) {
        this.sixthTimeMaintAct = sixthTimeMaintAct;
    }

    @Basic
    @Column(name = "PIO_LACK_LIST", nullable = true, length = 500)
    public String getPioLackList() {
        return pioLackList;
    }

    public void setPioLackList(String pioLackList) {
        this.pioLackList = pioLackList;
    }

    @Basic
    @Column(name = "ST_ARRIVAL", nullable = true)
    public Date getStArrival() {
        return stArrival;
    }

    public void setStArrival(Date stArrival) {
        this.stArrival = stArrival;
    }

    @Basic
    @Column(name = "ST_LOG_ID", nullable = true)
    public Long getStLogId() {
        return stLogId;
    }

    public void setStLogId(Long stLogId) {
        this.stLogId = stLogId;
    }

    @Basic
    @Column(name = "PAINT_IN_TIME", nullable = true, length = 12)
    public String getPaintInTime() {
        return paintInTime;
    }

    public void setPaintInTime(String paintInTime) {
        this.paintInTime = paintInTime;
    }

    @Basic
    @Column(name = "CL_DELIVERY", nullable = true)
    public Date getClDelivery() {
        return clDelivery;
    }

    public void setClDelivery(Date clDelivery) {
        this.clDelivery = clDelivery;
    }

    @Basic
    @Column(name = "CL_LOG_ID", nullable = true)
    public Long getClLogId() {
        return clLogId;
    }

    public void setClLogId(Long clLogId) {
        this.clLogId = clLogId;
    }

    @Basic
    @Column(name = "CL_TRUCK_TYPE_ID", nullable = true)
    public Long getClTruckTypeId() {
        return clTruckTypeId;
    }

    public void setClTruckTypeId(Long clTruckTypeId) {
        this.clTruckTypeId = clTruckTypeId;
    }

    @Basic
    @Column(name = "CL_TRUCK_ID", nullable = true)
    public Long getClTruckId() {
        return clTruckId;
    }

    public void setClTruckId(Long clTruckId) {
        this.clTruckId = clTruckId;
    }

    @Basic
    @Column(name = "SPCT_TRUCK_ID", nullable = true)
    public Long getSpctTruckId() {
        return spctTruckId;
    }

    public void setSpctTruckId(Long spctTruckId) {
        this.spctTruckId = spctTruckId;
    }

    @Basic
    @Column(name = "SPCT_TRUCK_TYPE_ID", nullable = true)
    public Long getSpctTruckTypeId() {
        return spctTruckTypeId;
    }

    public void setSpctTruckTypeId(Long spctTruckTypeId) {
        this.spctTruckTypeId = spctTruckTypeId;
    }

    @Basic
    @Column(name = "SPCT_LOG_ID", nullable = true)
    public Long getSpctLogId() {
        return spctLogId;
    }

    public void setSpctLogId(Long spctLogId) {
        this.spctLogId = spctLogId;
    }

    @Basic
    @Column(name = "SPCT_DELIVERY_DATE", nullable = true)
    public Date getSpctDeliveryDate() {
        return spctDeliveryDate;
    }

    public void setSpctDeliveryDate(Date spctDeliveryDate) {
        this.spctDeliveryDate = spctDeliveryDate;
    }

    @Basic
    @Column(name = "TMAP_PAPER_INVOICE_NO", nullable = true, length = 50)
    public String getTmapPaperInvoiceNo() {
        return tmapPaperInvoiceNo;
    }

    public void setTmapPaperInvoiceNo(String tmapPaperInvoiceNo) {
        this.tmapPaperInvoiceNo = tmapPaperInvoiceNo;
    }

    @Basic
    @Column(name = "TMAP_PAPER_INVOICE_DATE", nullable = true)
    public Date getTmapPaperInvoiceDate() {
        return tmapPaperInvoiceDate;
    }

    public void setTmapPaperInvoiceDate(Date tmapPaperInvoiceDate) {
        this.tmapPaperInvoiceDate = tmapPaperInvoiceDate;
    }

    @Basic
    @Column(name = "CUSTOM_START_PLAN", nullable = true)
    public Date getCustomStartPlan() {
        return customStartPlan;
    }

    public void setCustomStartPlan(Date customStartPlan) {
        this.customStartPlan = customStartPlan;
    }

    @Basic
    @Column(name = "VR_CHECK_PLAN", nullable = true)
    public Date getVrCheckPlan() {
        return vrCheckPlan;
    }

    public void setVrCheckPlan(Date vrCheckPlan) {
        this.vrCheckPlan = vrCheckPlan;
    }

    @Basic
    @Column(name = "CUSTOM_COMPLETE_PLAN", nullable = true)
    public Date getCustomCompletePlan() {
        return customCompletePlan;
    }

    public void setCustomCompletePlan(Date customCompletePlan) {
        this.customCompletePlan = customCompletePlan;
    }

    @Basic
    @Column(name = "CUSTOM_START_ACT", nullable = true)
    public Date getCustomStartAct() {
        return customStartAct;
    }

    public void setCustomStartAct(Date customStartAct) {
        this.customStartAct = customStartAct;
    }

    @Basic
    @Column(name = "VR_CHECK_ACT", nullable = true)
    public Date getVrCheckAct() {
        return vrCheckAct;
    }

    public void setVrCheckAct(Date vrCheckAct) {
        this.vrCheckAct = vrCheckAct;
    }

    @Basic
    @Column(name = "CUSTOM_COMPLETE_ACT", nullable = true)
    public Date getCustomCompleteAct() {
        return customCompleteAct;
    }

    public void setCustomCompleteAct(Date customCompleteAct) {
        this.customCompleteAct = customCompleteAct;
    }

    @Basic
    @Column(name = "CUSTOM_ACT_LT", nullable = true)
    public Long getCustomActLt() {
        return customActLt;
    }

    public void setCustomActLt(Long customActLt) {
        this.customActLt = customActLt;
    }

    @Basic
    @Column(name = "COQ_RECEIVE_DATE", nullable = true)
    public Date getCoqReceiveDate() {
        return coqReceiveDate;
    }

    public void setCoqReceiveDate(Date coqReceiveDate) {
        this.coqReceiveDate = coqReceiveDate;
    }

    @Basic
    @Column(name = "HOMOLOGATION", nullable = true, length = 1)
    public String getHomologation() {
        return homologation;
    }

    public void setHomologation(String homologation) {
        this.homologation = homologation;
    }

    @Basic
    @Column(name = "MARINE_MODE_ID", nullable = true)
    public Long getMarineModeId() {
        return marineModeId;
    }

    public void setMarineModeId(Long marineModeId) {
        this.marineModeId = marineModeId;
    }

    @Basic
    @Column(name = "DEPART_PORT_ID", nullable = true)
    public Long getDepartPortId() {
        return departPortId;
    }

    public void setDepartPortId(Long departPortId) {
        this.departPortId = departPortId;
    }

    @Basic
    @Column(name = "ARIVAL_PORT_ID", nullable = true, length = 20)
    public String getArivalPortId() {
        return arivalPortId;
    }

    public void setArivalPortId(String arivalPortId) {
        this.arivalPortId = arivalPortId;
    }

    @Basic
    @Column(name = "PORT_ETD", nullable = true)
    public Date getPortEtd() {
        return portEtd;
    }

    public void setPortEtd(Date portEtd) {
        this.portEtd = portEtd;
    }

    @Basic
    @Column(name = "PORT_ETA", nullable = true)
    public Date getPortEta() {
        return portEta;
    }

    public void setPortEta(Date portEta) {
        this.portEta = portEta;
    }

    @Basic
    @Column(name = "ACT_DEPART_DATE", nullable = true)
    public Date getActDepartDate() {
        return actDepartDate;
    }

    public void setActDepartDate(Date actDepartDate) {
        this.actDepartDate = actDepartDate;
    }

    @Basic
    @Column(name = "ACT_ARRIVAL_DATE", nullable = true)
    public Date getActArrivalDate() {
        return actArrivalDate;
    }

    public void setActArrivalDate(Date actArrivalDate) {
        this.actArrivalDate = actArrivalDate;
    }

    @Basic
    @Column(name = "VR_CHECK_ACT_LT", nullable = true)
    public Long getVrCheckActLt() {
        return vrCheckActLt;
    }

    public void setVrCheckActLt(Long vrCheckActLt) {
        this.vrCheckActLt = vrCheckActLt;
    }

    @Basic
    @Column(name = "INTER_PORT_ASSIGN_DATE", nullable = true)
    public Date getInterPortAssignDate() {
        return interPortAssignDate;
    }

    public void setInterPortAssignDate(Date interPortAssignDate) {
        this.interPortAssignDate = interPortAssignDate;
    }

    @Basic
    @Column(name = "INTER_ASSIGN_DES_ID", nullable = true, length = 50)
    public String getInterAssignDesId() {
        return interAssignDesId;
    }

    public void setInterAssignDesId(String interAssignDesId) {
        this.interAssignDesId = interAssignDesId;
    }

    @Basic
    @Column(name = "INTER_PORT_DISPATCH_DATE", nullable = true)
    public Date getInterPortDispatchDate() {
        return interPortDispatchDate;
    }

    public void setInterPortDispatchDate(Date interPortDispatchDate) {
        this.interPortDispatchDate = interPortDispatchDate;
    }

    @Basic
    @Column(name = "INTER_TRANS_ROUTE_ID", nullable = true, length = 50)
    public String getInterTransRouteId() {
        return interTransRouteId;
    }

    public void setInterTransRouteId(String interTransRouteId) {
        this.interTransRouteId = interTransRouteId;
    }

    @Basic
    @Column(name = "INTER_MEAN_TRANS_ID", nullable = true)
    public Long getInterMeanTransId() {
        return interMeanTransId;
    }

    public void setInterMeanTransId(Long interMeanTransId) {
        this.interMeanTransId = interMeanTransId;
    }

    @Basic
    @Column(name = "INTER_LOG_ID", nullable = true)
    public Long getInterLogId() {
        return interLogId;
    }

    public void setInterLogId(Long interLogId) {
        this.interLogId = interLogId;
    }

    @Basic
    @Column(name = "INTER_TRUCT_ID", nullable = true)
    public Long getInterTructId() {
        return interTructId;
    }

    public void setInterTructId(Long interTructId) {
        this.interTructId = interTructId;
    }

    @Basic
    @Column(name = "INTER_TRANS_COST", nullable = true)
    public Long getInterTransCost() {
        return interTransCost;
    }

    public void setInterTransCost(Long interTransCost) {
        this.interTransCost = interTransCost;
    }

    @Basic
    @Column(name = "INTER_DES_ARRIVAL_DATE", nullable = true)
    public Date getInterDesArrivalDate() {
        return interDesArrivalDate;
    }

    public void setInterDesArrivalDate(Date interDesArrivalDate) {
        this.interDesArrivalDate = interDesArrivalDate;
    }

    @Basic
    @Column(name = "INTER_DES_ARRIVAL_TIME", nullable = true, length = 10)
    public String getInterDesArrivalTime() {
        return interDesArrivalTime;
    }

    public void setInterDesArrivalTime(String interDesArrivalTime) {
        this.interDesArrivalTime = interDesArrivalTime;
    }

    @Basic
    @Column(name = "SWAP_ASSIGN_DATE", nullable = true)
    public Date getSwapAssignDate() {
        return swapAssignDate;
    }

    public void setSwapAssignDate(Date swapAssignDate) {
        this.swapAssignDate = swapAssignDate;
    }

    @Basic
    @Column(name = "SWAP_ASSIGN_DES_ID", nullable = true, length = 20)
    public String getSwapAssignDesId() {
        return swapAssignDesId;
    }

    public void setSwapAssignDesId(String swapAssignDesId) {
        this.swapAssignDesId = swapAssignDesId;
    }

    @Basic
    @Column(name = "SWAP_ROUTE_ID", nullable = true, length = 50)
    public String getSwapRouteId() {
        return swapRouteId;
    }

    public void setSwapRouteId(String swapRouteId) {
        this.swapRouteId = swapRouteId;
    }

    @Basic
    @Column(name = "SWAP_REASON", nullable = true, length = 255)
    public String getSwapReason() {
        return swapReason;
    }

    public void setSwapReason(String swapReason) {
        this.swapReason = swapReason;
    }

    @Basic
    @Column(name = "SWAP_LOG_ID", nullable = true)
    public Long getSwapLogId() {
        return swapLogId;
    }

    public void setSwapLogId(Long swapLogId) {
        this.swapLogId = swapLogId;
    }

    @Basic
    @Column(name = "SWAP_TRUCT_TYPE_ID", nullable = true)
    public Long getSwapTructTypeId() {
        return swapTructTypeId;
    }

    public void setSwapTructTypeId(Long swapTructTypeId) {
        this.swapTructTypeId = swapTructTypeId;
    }

    @Basic
    @Column(name = "SWAP_TRUCT_ID", nullable = true)
    public Long getSwapTructId() {
        return swapTructId;
    }

    public void setSwapTructId(Long swapTructId) {
        this.swapTructId = swapTructId;
    }

    @Basic
    @Column(name = "SWAP_TRANSPORT_COST", nullable = true)
    public Long getSwapTransportCost() {
        return swapTransportCost;
    }

    public void setSwapTransportCost(Long swapTransportCost) {
        this.swapTransportCost = swapTransportCost;
    }

    @Basic
    @Column(name = "SWAP_DES_ARRIVAL_DATE", nullable = true)
    public Date getSwapDesArrivalDate() {
        return swapDesArrivalDate;
    }

    public void setSwapDesArrivalDate(Date swapDesArrivalDate) {
        this.swapDesArrivalDate = swapDesArrivalDate;
    }

    @Basic
    @Column(name = "ORDER_MONTH", nullable = true)
    public Date getOrderMonth() {
        return orderMonth;
    }

    public void setOrderMonth(Date orderMonth) {
        this.orderMonth = orderMonth;
    }

    @Basic
    @Column(name = "PRODUCTION_DATE", nullable = true)
    public Date getProductionDate() {
        return productionDate;
    }

    public void setProductionDate(Date productionDate) {
        this.productionDate = productionDate;
    }

    @Basic
    @Column(name = "DLR_DISPATCH_PLAN", nullable = true)
    public Date getDlrDispatchPlan() {
        return dlrDispatchPlan;
    }

    public void setDlrDispatchPlan(Date dlrDispatchPlan) {
        this.dlrDispatchPlan = dlrDispatchPlan;
    }

    @Basic
    @Column(name = "DLR_LOG_ID", nullable = true)
    public Long getDlrLogId() {
        return dlrLogId;
    }

    public void setDlrLogId(Long dlrLogId) {
        this.dlrLogId = dlrLogId;
    }

    @Basic
    @Column(name = "DLR_TRUCK_TYPE_ID", nullable = true)
    public Long getDlrTruckTypeId() {
        return dlrTruckTypeId;
    }

    public void setDlrTruckTypeId(Long dlrTruckTypeId) {
        this.dlrTruckTypeId = dlrTruckTypeId;
    }

    @Basic
    @Column(name = "DLR_TRUCK_ID", nullable = true)
    public Long getDlrTruckId() {
        return dlrTruckId;
    }

    public void setDlrTruckId(Long dlrTruckId) {
        this.dlrTruckId = dlrTruckId;
    }

    @Basic
    @Column(name = "DLR_TRANS_ROUTE_ID", nullable = true, length = 20)
    public String getDlrTransRouteId() {
        return dlrTransRouteId;
    }

    public void setDlrTransRouteId(String dlrTransRouteId) {
        this.dlrTransRouteId = dlrTransRouteId;
    }

    @Basic
    @Column(name = "STOCK_DAY", nullable = true)
    public Long getStockDay() {
        return stockDay;
    }

    public void setStockDay(Long stockDay) {
        this.stockDay = stockDay;
    }

    @Basic
    @Column(name = "STOCK_FEE_EXPORT", nullable = true)
    public Long getStockFeeExport() {
        return stockFeeExport;
    }

    public void setStockFeeExport(Long stockFeeExport) {
        this.stockFeeExport = stockFeeExport;
    }

    @Basic
    @Column(name = "INTERIOR_COL_ID", nullable = true)
    public Long getInteriorColId() {
        return interiorColId;
    }

    public void setInteriorColId(Long interiorColId) {
        this.interiorColId = interiorColId;
    }

    @Basic
    @Column(name = "VEHICLE_STATUS", nullable = true)
    public Long getVehicleStatus() {
        return vehicleStatus;
    }

    public void setVehicleStatus(Long vehicleStatus) {
        this.vehicleStatus = vehicleStatus;
    }

    @Basic
    @Column(name = "ESTIMATE_ALLOCATION_MONTH", nullable = true)
    public Date getEstimateAllocationMonth() {
        return estimateAllocationMonth;
    }

    public void setEstimateAllocationMonth(Date estimateAllocationMonth) {
        this.estimateAllocationMonth = estimateAllocationMonth;
    }

    @Basic
    @Column(name = "REMARK", nullable = true, length = 2000)
    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Basic
    @Column(name = "CUSTOM_TMV_ARR_DATE", nullable = true)
    public Date getCustomTmvArrDate() {
        return customTmvArrDate;
    }

    public void setCustomTmvArrDate(Date customTmvArrDate) {
        this.customTmvArrDate = customTmvArrDate;
    }

    @Basic
    @Column(name = "INTER_PORT_DISPATCH_PLAN", nullable = true)
    public Date getInterPortDispatchPlan() {
        return interPortDispatchPlan;
    }

    public void setInterPortDispatchPlan(Date interPortDispatchPlan) {
        this.interPortDispatchPlan = interPortDispatchPlan;
    }

    @Basic
    @Column(name = "INTER_PORT_DISPATCH_DELAY", nullable = true)
    public Long getInterPortDispatchDelay() {
        return interPortDispatchDelay;
    }

    public void setInterPortDispatchDelay(Long interPortDispatchDelay) {
        this.interPortDispatchDelay = interPortDispatchDelay;
    }

    @Basic
    @Column(name = "INTER_PORT_DELAY_REASON", nullable = true, length = 255)
    public String getInterPortDelayReason() {
        return interPortDelayReason;
    }

    public void setInterPortDelayReason(String interPortDelayReason) {
        this.interPortDelayReason = interPortDelayReason;
    }

    @Basic
    @Column(name = "INTER_DES_ARRIVAL_DELAY", nullable = true)
    public Long getInterDesArrivalDelay() {
        return interDesArrivalDelay;
    }

    public void setInterDesArrivalDelay(Long interDesArrivalDelay) {
        this.interDesArrivalDelay = interDesArrivalDelay;
    }

    @Basic
    @Column(name = "INTER_DES_ARRIVAL_PLAN", nullable = true)
    public Date getInterDesArrivalPlan() {
        return interDesArrivalPlan;
    }

    public void setInterDesArrivalPlan(Date interDesArrivalPlan) {
        this.interDesArrivalPlan = interDesArrivalPlan;
    }

    @Basic
    @Column(name = "INTER_DES_ARRIVAL_DELAY_REASON", nullable = true, length = 255)
    public String getInterDesArrivalDelayReason() {
        return interDesArrivalDelayReason;
    }

    public void setInterDesArrivalDelayReason(String interDesArrivalDelayReason) {
        this.interDesArrivalDelayReason = interDesArrivalDelayReason;
    }

    @Basic
    @Column(name = "SWAPPING_DISPATCH_PLAN", nullable = true)
    public Date getSwappingDispatchPlan() {
        return swappingDispatchPlan;
    }

    public void setSwappingDispatchPlan(Date swappingDispatchPlan) {
        this.swappingDispatchPlan = swappingDispatchPlan;
    }

    @Basic
    @Column(name = "SWAPPING_DISPATCH_DATE", nullable = true)
    public Date getSwappingDispatchDate() {
        return swappingDispatchDate;
    }

    public void setSwappingDispatchDate(Date swappingDispatchDate) {
        this.swappingDispatchDate = swappingDispatchDate;
    }

    @Basic
    @Column(name = "SWAPPING_DES_ARRIVAL_PLAN", nullable = true)
    public Date getSwappingDesArrivalPlan() {
        return swappingDesArrivalPlan;
    }

    public void setSwappingDesArrivalPlan(Date swappingDesArrivalPlan) {
        this.swappingDesArrivalPlan = swappingDesArrivalPlan;
    }

    @Basic
    @Column(name = "SWAPPING_ARRIVAL_DELAY", nullable = true)
    public Long getSwappingArrivalDelay() {
        return swappingArrivalDelay;
    }

    public void setSwappingArrivalDelay(Long swappingArrivalDelay) {
        this.swappingArrivalDelay = swappingArrivalDelay;
    }

    @Basic
    @Column(name = "SWAPPING_ARRIVAL_DELAY_REASON", nullable = true, length = 255)
    public String getSwappingArrivalDelayReason() {
        return swappingArrivalDelayReason;
    }

    public void setSwappingArrivalDelayReason(String swappingArrivalDelayReason) {
        this.swappingArrivalDelayReason = swappingArrivalDelayReason;
    }

    @Basic
    @Column(name = "DLR_ASS_PLAN_DATE", nullable = true)
    public Date getDlrAssPlanDate() {
        return dlrAssPlanDate;
    }

    public void setDlrAssPlanDate(Date dlrAssPlanDate) {
        this.dlrAssPlanDate = dlrAssPlanDate;
    }

    @Basic
    @Column(name = "DLR_ASS_ACTUAL_DATE", nullable = true)
    public Date getDlrAssActualDate() {
        return dlrAssActualDate;
    }

    public void setDlrAssActualDate(Date dlrAssActualDate) {
        this.dlrAssActualDate = dlrAssActualDate;
    }

    @Basic
    @Column(name = "PDI_OWNER_MANUAL", nullable = true, length = 20)
    public String getPdiOwnerManual() {
        return pdiOwnerManual;
    }

    public void setPdiOwnerManual(String pdiOwnerManual) {
        this.pdiOwnerManual = pdiOwnerManual;
    }

    @Basic
    @Column(name = "DAMAGE_FOUND_DATE", nullable = true)
    public Date getDamageFoundDate() {
        return damageFoundDate;
    }

    public void setDamageFoundDate(Date damageFoundDate) {
        this.damageFoundDate = damageFoundDate;
    }

    @Basic
    @Column(name = "ML_DELAY_DISPATCH_REASON", nullable = true, length = 255)
    public String getMlDelayDispatchReason() {
        return mlDelayDispatchReason;
    }

    public void setMlDelayDispatchReason(String mlDelayDispatchReason) {
        this.mlDelayDispatchReason = mlDelayDispatchReason;
    }

    @Basic
    @Column(name = "ML_DELAY_DISPATCH", nullable = true)
    public Long getMlDelayDispatch() {
        return mlDelayDispatch;
    }

    public void setMlDelayDispatch(Long mlDelayDispatch) {
        this.mlDelayDispatch = mlDelayDispatch;
    }

    @Basic
    @Column(name = "PDI_TRANSPORT_ROUTE", nullable = true, length = 255)
    public String getPdiTransportRoute() {
        return pdiTransportRoute;
    }

    public void setPdiTransportRoute(String pdiTransportRoute) {
        this.pdiTransportRoute = pdiTransportRoute;
    }

    @Basic
    @Column(name = "DAMAGE_REPAIR_DATE", nullable = true)
    public Date getDamageRepairDate() {
        return damageRepairDate;
    }

    public void setDamageRepairDate(Date damageRepairDate) {
        this.damageRepairDate = damageRepairDate;
    }

    @Basic
    @Column(name = "DLR_DISPATCH_DATE", nullable = true)
    public Date getDlrDispatchDate() {
        return dlrDispatchDate;
    }

    public void setDlrDispatchDate(Date dlrDispatchDate) {
        this.dlrDispatchDate = dlrDispatchDate;
    }

    @Basic
    @Column(name = "DLR_DISPATCH_TIME", nullable = true, length = 20)
    public String getDlrDispatchTime() {
        return dlrDispatchTime;
    }

    public void setDlrDispatchTime(String dlrDispatchTime) {
        this.dlrDispatchTime = dlrDispatchTime;
    }

    @Basic
    @Column(name = "BL_NO", nullable = true, length = 50)
    public String getBlNo() {
        return blNo;
    }

    public void setBlNo(String blNo) {
        this.blNo = blNo;
    }

    @Basic
    @Column(name = "CUSTOM_SHEET_NO", nullable = true, length = 50)
    public String getCustomSheetNo() {
        return customSheetNo;
    }

    public void setCustomSheetNo(String customSheetNo) {
        this.customSheetNo = customSheetNo;
    }

    @Basic
    @Column(name = "PRICE_CONSULTING_DATE", nullable = true)
    public Date getPriceConsultingDate() {
        return priceConsultingDate;
    }

    public void setPriceConsultingDate(Date priceConsultingDate) {
        this.priceConsultingDate = priceConsultingDate;
    }

    @Basic
    @Column(name = "TMV_OC_SIGNING_DATE", nullable = true)
    public Date getTmvOcSigningDate() {
        return tmvOcSigningDate;
    }

    public void setTmvOcSigningDate(Date tmvOcSigningDate) {
        this.tmvOcSigningDate = tmvOcSigningDate;
    }

    @Basic
    @Column(name = "CUSTOM_OC_SIGNING_DATE", nullable = true)
    public Date getCustomOcSigningDate() {
        return customOcSigningDate;
    }

    public void setCustomOcSigningDate(Date customOcSigningDate) {
        this.customOcSigningDate = customOcSigningDate;
    }

    @Basic
    @Column(name = "CUSTOM_REMARK", nullable = true, length = 255)
    public String getCustomRemark() {
        return customRemark;
    }

    public void setCustomRemark(String customRemark) {
        this.customRemark = customRemark;
    }

    @Basic
    @Column(name = "INVOICE_REQUEST_DATE", nullable = true)
    public Date getInvoiceRequestDate() {
        return invoiceRequestDate;
    }

    public void setInvoiceRequestDate(Date invoiceRequestDate) {
        this.invoiceRequestDate = invoiceRequestDate;
    }

    @Basic
    @Column(name = "DLR_DELAY", nullable = true, length = 50)
    public String getDlrDelay() {
        return dlrDelay;
    }

    public void setDlrDelay(String dlrDelay) {
        this.dlrDelay = dlrDelay;
    }

    @Basic
    @Column(name = "VR_CERTIFICATE_RECEIVE_DATE", nullable = true)
    public Date getVrCertificateReceiveDate() {
        return vrCertificateReceiveDate;
    }

    public void setVrCertificateReceiveDate(Date vrCertificateReceiveDate) {
        this.vrCertificateReceiveDate = vrCertificateReceiveDate;
    }

    @Basic
    @Column(name = "CBU_PRODUCTION_MONTH", nullable = true)
    public Date getCbuProductionMonth() {
        return cbuProductionMonth;
    }

    public void setCbuProductionMonth(Date cbuProductionMonth) {
        this.cbuProductionMonth = cbuProductionMonth;
    }

    @Basic
    @Column(name = "ORIGINAL_LO_PLAN_DATE", nullable = true)
    public Date getOriginalLoPlanDate() {
        return originalLoPlanDate;
    }

    public void setOriginalLoPlanDate(Date originalLoPlanDate) {
        this.originalLoPlanDate = originalLoPlanDate;
    }

    @Basic
    @Column(name = "LO_DELAY_REASON", nullable = true, length = 2000)
    public String getLoDelayReason() {
        return loDelayReason;
    }

    public void setLoDelayReason(String loDelayReason) {
        this.loDelayReason = loDelayReason;
    }

    @Basic
    @Column(name = "DLR_ACTUAL_ARRIVAL_DATE", nullable = true)
    public Date getDlrActualArrivalDate() {
        return dlrActualArrivalDate;
    }

    public void setDlrActualArrivalDate(Date dlrActualArrivalDate) {
        this.dlrActualArrivalDate = dlrActualArrivalDate;
    }

    @Basic
    @Column(name = "ORIGINAL_ARRIVAL_PLAN", nullable = true)
    public Date getOriginalArrivalPlan() {
        return originalArrivalPlan;
    }

    public void setOriginalArrivalPlan(Date originalArrivalPlan) {
        this.originalArrivalPlan = originalArrivalPlan;
    }

    @Basic
    @Column(name = "DELAY_REASON_ORI_ARR_PLAN", nullable = true, length = 2000)
    public String getDelayReasonOriArrPlan() {
        return delayReasonOriArrPlan;
    }

    public void setDelayReasonOriArrPlan(String delayReasonOriArrPlan) {
        this.delayReasonOriArrPlan = delayReasonOriArrPlan;
    }

    @Basic
    @Column(name = "DOC_SENDING_VENDOR", nullable = true, length = 20)
    public String getDocSendingVendor() {
        return docSendingVendor;
    }

    public void setDocSendingVendor(String docSendingVendor) {
        this.docSendingVendor = docSendingVendor;
    }

    @Basic
    @Column(name = "SWAP_REMARK", nullable = true, length = 1000)
    public String getSwapRemark() {
        return swapRemark;
    }

    public void setSwapRemark(String swapRemark) {
        this.swapRemark = swapRemark;
    }

    @Basic
    @Column(name = "YARD_AREA_ID", nullable = true)
    public Long getYardAreaId() {
        return yardAreaId;
    }

    public void setYardAreaId(Long yardAreaId) {
        this.yardAreaId = yardAreaId;
    }

    @Basic
    @Column(name = "YARD_LOC_ID", nullable = true)
    public Long getYardLocId() {
        return yardLocId;
    }

    public void setYardLocId(Long yardLocId) {
        this.yardLocId = yardLocId;
    }

    @Basic
    @Column(name = "ORIGINAL_LO_PLAN_TIME", nullable = true, length = 10)
    public String getOriginalLoPlanTime() {
        return originalLoPlanTime;
    }

    public void setOriginalLoPlanTime(String originalLoPlanTime) {
        this.originalLoPlanTime = originalLoPlanTime;
    }

    @Basic
    @Column(name = "TEMP_LICENSE_DATE", nullable = true)
    public Date getTempLicenseDate() {
        return tempLicenseDate;
    }

    public void setTempLicenseDate(Date tempLicenseDate) {
        this.tempLicenseDate = tempLicenseDate;
    }

    @Basic
    @Column(name = "DIESEL", nullable = true)
    public Long getDiesel() {
        return diesel;
    }

    public void setDiesel(Long diesel) {
        this.diesel = diesel;
    }

    @Basic
    @Column(name = "TRANSPORT_WAY", nullable = true, length = 2)
    public String getTransportWay() {
        return transportWay;
    }

    public void setTransportWay(String transportWay) {
        this.transportWay = transportWay;
    }

    @Basic
    @Column(name = "PORT_TRANSPORT_WAY", nullable = true, length = 2)
    public String getPortTransportWay() {
        return portTransportWay;
    }

    public void setPortTransportWay(String portTransportWay) {
        this.portTransportWay = portTransportWay;
    }

    @Basic
    @Column(name = "DLR_TRANSPORT_WAY", nullable = true, length = 2)
    public String getDlrTransportWay() {
        return dlrTransportWay;
    }

    public void setDlrTransportWay(String dlrTransportWay) {
        this.dlrTransportWay = dlrTransportWay;
    }

    @Basic
    @Column(name = "DLR_DISPATCH_PLAN_TIME", nullable = true, length = 12)
    public String getDlrDispatchPlanTime() {
        return dlrDispatchPlanTime;
    }

    public void setDlrDispatchPlanTime(String dlrDispatchPlanTime) {
        this.dlrDispatchPlanTime = dlrDispatchPlanTime;
    }

    @Basic
    @Column(name = "SIDE_AIRBAG_LH", nullable = true, length = 255)
    public String getSideAirbagLh() {
        return sideAirbagLh;
    }

    public void setSideAirbagLh(String sideAirbagLh) {
        this.sideAirbagLh = sideAirbagLh;
    }

    @Basic
    @Column(name = "SIDE_AIRBAG_RH", nullable = true, length = 255)
    public String getSideAirbagRh() {
        return sideAirbagRh;
    }

    public void setSideAirbagRh(String sideAirbagRh) {
        this.sideAirbagRh = sideAirbagRh;
    }

    @Basic
    @Column(name = "KNEE_AIRBAG", nullable = true, length = 255)
    public String getKneeAirbag() {
        return kneeAirbag;
    }

    public void setKneeAirbag(String kneeAirbag) {
        this.kneeAirbag = kneeAirbag;
    }

    @Basic
    @Column(name = "SEQ_LEXUS", nullable = true)
    public Long getSeqLexus() {
        return seqLexus;
    }

    public void setSeqLexus(Long seqLexus) {
        this.seqLexus = seqLexus;
    }

    @Basic
    @Column(name = "SELF_DRIVING_TRIP_REQUEST", nullable = true)
    public Long getSelfDrivingTripRequest() {
        return selfDrivingTripRequest;
    }

    public void setSelfDrivingTripRequest(Long selfDrivingTripRequest) {
        this.selfDrivingTripRequest = selfDrivingTripRequest;
    }

    @Basic
    @Column(name = "MODIFIED_DATE_DISPATCH_PLAN", nullable = true)
    public Date getModifiedDateDispatchPlan() {
        return modifiedDateDispatchPlan;
    }

    public void setModifiedDateDispatchPlan(Date modifiedDateDispatchPlan) {
        this.modifiedDateDispatchPlan = modifiedDateDispatchPlan;
    }

    @Basic
    @Column(name = "DLR_RQ_COL_ID", nullable = true)
    public Long getDlrRqColId() {
        return dlrRqColId;
    }

    public void setDlrRqColId(Long dlrRqColId) {
        this.dlrRqColId = dlrRqColId;
    }

    @Basic
    @Column(name = "DLR_COL_DEADLINE", nullable = true)
    public Date getDlrColDeadline() {
        return dlrColDeadline;
    }

    public void setDlrColDeadline(Date dlrColDeadline) {
        this.dlrColDeadline = dlrColDeadline;
    }

    @Basic
    @Column(name = "TFS_CREATE_DATE", nullable = true)
    public Timestamp getTfsCreateDate() {
        return tfsCreateDate;
    }

    public void setTfsCreateDate(Timestamp tfsCreateDate) {
        this.tfsCreateDate = tfsCreateDate;
    }

    @Basic
    @Column(name = "TMSS_NO", nullable = true, length = 255)
    public String getTmssNo() {
        return tmssNo;
    }

    public void setTmssNo(String tmssNo) {
        this.tmssNo = tmssNo;
    }

    @Basic
    @Column(name = "LATEST_LO_PLAN_DATE", nullable = true)
    public Date getLatestLoPlanDate() {
        return latestLoPlanDate;
    }

    public void setLatestLoPlanDate(Date latestLoPlanDate) {
        this.latestLoPlanDate = latestLoPlanDate;
    }

    @Basic
    @Column(name = "MODIFY_DATE_ANDON", nullable = true)
    public Date getModifyDateAndon() {
        return modifyDateAndon;
    }

    public void setModifyDateAndon(Date modifyDateAndon) {
        this.modifyDateAndon = modifyDateAndon;
    }

    @Basic
    @Column(name = "ENG_CODE", nullable = true, length = 20)
    public String getEngCode() {
        return engCode;
    }

    public void setEngCode(String engCode) {
        this.engCode = engCode;
    }

    @Basic
    @Column(name = "DLR_AT_ID", nullable = true)
    public Long getDlrAtId() {
        return dlrAtId;
    }

    public void setDlrAtId(Long dlrAtId) {
        this.dlrAtId = dlrAtId;
    }

    @Basic
    @Column(name = "PAY_STATUS", nullable = true, length = 20)
    public String getPayStatus() {
        return payStatus;
    }

    public void setPayStatus(String payStatus) {
        this.payStatus = payStatus;
    }

    @Basic
    @Column(name = "MODIFY_DATE_YARD_SYSTEM", nullable = true)
    public Date getModifyDateYardSystem() {
        return modifyDateYardSystem;
    }

    public void setModifyDateYardSystem(Date modifyDateYardSystem) {
        this.modifyDateYardSystem = modifyDateYardSystem;
    }

    @Basic
    @Column(name = "TRUCK_LOCATION", nullable = true, length = 250)
    public String getTruckLocation() {
        return truckLocation;
    }

    public void setTruckLocation(String truckLocation) {
        this.truckLocation = truckLocation;
    }

    @Basic
    @Column(name = "STANDBY_LOCATION_PDI", nullable = true, length = 250)
    public String getStandbyLocationPdi() {
        return standbyLocationPdi;
    }

    public void setStandbyLocationPdi(String standbyLocationPdi) {
        this.standbyLocationPdi = standbyLocationPdi;
    }

    @Basic
    @Column(name = "ML_MOVING_YARD_DATE", nullable = true)
    public Date getMlMovingYardDate() {
        return mlMovingYardDate;
    }

    public void setMlMovingYardDate(Date mlMovingYardDate) {
        this.mlMovingYardDate = mlMovingYardDate;
    }

    @Basic
    @Column(name = "ML_MOVING_YARD_TIME", nullable = true)
    public Long getMlMovingYardTime() {
        return mlMovingYardTime;
    }

    public void setMlMovingYardTime(Long mlMovingYardTime) {
        this.mlMovingYardTime = mlMovingYardTime;
    }

    @Basic
    @Column(name = "DRIVER_NAME", nullable = true, length = 2000)
    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    @Basic
    @Column(name = "MODIFIED_DATE_RQ_COL_ID", nullable = true)
    public Date getModifiedDateRqColId() {
        return modifiedDateRqColId;
    }

    public void setModifiedDateRqColId(Date modifiedDateRqColId) {
        this.modifiedDateRqColId = modifiedDateRqColId;
    }

    @Basic
    @Column(name = "MODIFY_DATE_COL_DEADLINE", nullable = true)
    public Date getModifyDateColDeadline() {
        return modifyDateColDeadline;
    }

    public void setModifyDateColDeadline(Date modifyDateColDeadline) {
        this.modifyDateColDeadline = modifyDateColDeadline;
    }

    @Basic
    @Column(name = "PDI_TIME", nullable = true)
    public Long getPdiTime() {
        return pdiTime;
    }

    public void setPdiTime(Long pdiTime) {
        this.pdiTime = pdiTime;
    }

    @Basic
    @Column(name = "P_BEGIN_INSTALL_TIME", nullable = true, length = 12)
    public String getpBeginInstallTime() {
        return pBeginInstallTime;
    }

    public void setpBeginInstallTime(String pBeginInstallTime) {
        this.pBeginInstallTime = pBeginInstallTime;
    }

    @Basic
    @Column(name = "PIO_BAY", nullable = true, length = 225)
    public String getPioBay() {
        return pioBay;
    }

    public void setPioBay(String pioBay) {
        this.pioBay = pioBay;
    }

    @Basic
    @Column(name = "PIO_MEMBER", nullable = true, length = 225)
    public String getPioMember() {
        return pioMember;
    }

    public void setPioMember(String pioMember) {
        this.pioMember = pioMember;
    }

    @Basic
    @Column(name = "DISPATCH_CHANGE_REQ_DATE", nullable = true)
    public Date getDispatchChangeReqDate() {
        return dispatchChangeReqDate;
    }

    public void setDispatchChangeReqDate(Date dispatchChangeReqDate) {
        this.dispatchChangeReqDate = dispatchChangeReqDate;
    }

    @Basic
    @Column(name = "DISPATCH_CHANGE_SEND_DATE", nullable = true)
    public Date getDispatchChangeSendDate() {
        return dispatchChangeSendDate;
    }

    public void setDispatchChangeSendDate(Date dispatchChangeSendDate) {
        this.dispatchChangeSendDate = dispatchChangeSendDate;
    }

    @Basic
    @Column(name = "ADVANCE_REQUEST_DATE", nullable = true)
    public Date getAdvanceRequestDate() {
        return advanceRequestDate;
    }

    public void setAdvanceRequestDate(Date advanceRequestDate) {
        this.advanceRequestDate = advanceRequestDate;
    }

    @Basic
    @Column(name = "DISPATCH_CHANGE_STATUS", nullable = true, length = 20)
    public String getDispatchChangeStatus() {
        return dispatchChangeStatus;
    }

    public void setDispatchChangeStatus(String dispatchChangeStatus) {
        this.dispatchChangeStatus = dispatchChangeStatus;
    }

    @Basic
    @Column(name = "NEW_DISPATCH_PLAN_DATE", nullable = true)
    public Date getNewDispatchPlanDate() {
        return newDispatchPlanDate;
    }

    public void setNewDispatchPlanDate(Date newDispatchPlanDate) {
        this.newDispatchPlanDate = newDispatchPlanDate;
    }

    @Basic
    @Column(name = "DISPATCH_CHANGE_CONFIRM_DATE", nullable = true)
    public Date getDispatchChangeConfirmDate() {
        return dispatchChangeConfirmDate;
    }

    public void setDispatchChangeConfirmDate(Date dispatchChangeConfirmDate) {
        this.dispatchChangeConfirmDate = dispatchChangeConfirmDate;
    }

    @Basic
    @Column(name = "CBU_DOC_DELIVERY", nullable = true)
    public Date getCbuDocDelivery() {
        return cbuDocDelivery;
    }

    public void setCbuDocDelivery(Date cbuDocDelivery) {
        this.cbuDocDelivery = cbuDocDelivery;
    }

    @Basic
    @Column(name = "PRINT_INV_DESC_ANDON", nullable = true, length = 50)
    public String getPrintInvDescAndon() {
        return printInvDescAndon;
    }

    public void setPrintInvDescAndon(String printInvDescAndon) {
        this.printInvDescAndon = printInvDescAndon;
    }

    @Basic
    @Column(name = "P_FINISH_INSTALL_TIME", nullable = true, length = 12)
    public String getpFinishInstallTime() {
        return pFinishInstallTime;
    }

    public void setpFinishInstallTime(String pFinishInstallTime) {
        this.pFinishInstallTime = pFinishInstallTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SaVehiclePO that = (SaVehiclePO) o;
        return id == that.id &&
                Objects.equals(dlrId, that.dlrId) &&
                Objects.equals(frameNo, that.frameNo) &&
                Objects.equals(vin, that.vin) &&
                Objects.equals(engineNo, that.engineNo) &&
                Objects.equals(accId, that.accId) &&
                Objects.equals(cfuId, that.cfuId) &&
                Objects.equals(graId, that.graId) &&
                Objects.equals(locId, that.locId) &&
                Objects.equals(truId, that.truId) &&
                Objects.equals(installDate, that.installDate) &&
                Objects.equals(painInDate, that.painInDate) &&
                Objects.equals(ccrLotNo, that.ccrLotNo) &&
                Objects.equals(ccrNoInLot, that.ccrNoInLot) &&
                Objects.equals(ccrBodyNo, that.ccrBodyNo) &&
                Objects.equals(ccrAIn, that.ccrAIn) &&
                Objects.equals(ccrAOut, that.ccrAOut) &&
                Objects.equals(keyCode, that.keyCode) &&
                Objects.equals(qcDriverNo, that.qcDriverNo) &&
                Objects.equals(qcPassengerNo, that.qcPassengerNo) &&
                Objects.equals(qcEngno, that.qcEngno) &&
                Objects.equals(qcTranmissionNo, that.qcTranmissionNo) &&
                Objects.equals(qcAxleNo, that.qcAxleNo) &&
                Objects.equals(qcLoshift, that.qcLoshift) &&
                Objects.equals(lineOffDate, that.lineOffDate) &&
                Objects.equals(assChangeFrom, that.assChangeFrom) &&
                Objects.equals(assChangeReason, that.assChangeReason) &&
                Objects.equals(assAlloMonth, that.assAlloMonth) &&
                Objects.equals(dlrPaymentPlan, that.dlrPaymentPlan) &&
                Objects.equals(rFPaymentDate, that.rFPaymentDate) &&
                Objects.equals(payInvoiceNo, that.payInvoiceNo) &&
                Objects.equals(payActualInvoiceDate, that.payActualInvoiceDate) &&
                Objects.equals(payVnAmount, that.payVnAmount) &&
                Objects.equals(payUsdAmount, that.payUsdAmount) &&
                Objects.equals(payHoldbackAmount, that.payHoldbackAmount) &&
                Objects.equals(payRFPayback, that.payRFPayback) &&
                Objects.equals(payDiscountAmount, that.payDiscountAmount) &&
                Objects.equals(documentTmvSituationId, that.documentTmvSituationId) &&
                Objects.equals(documentDeliveryDate, that.documentDeliveryDate) &&
                Objects.equals(documentDeliveryTime, that.documentDeliveryTime) &&
                Objects.equals(documentArrivalDate, that.documentArrivalDate) &&
                Objects.equals(documentArrivalTime, that.documentArrivalTime) &&
                Objects.equals(documentArrivalRemark, that.documentArrivalRemark) &&
                Objects.equals(documentDlrSituationId, that.documentDlrSituationId) &&
                Objects.equals(fleetPrice, that.fleetPrice) &&
                Objects.equals(mlDeliveryDate, that.mlDeliveryDate) &&
                Objects.equals(mlDeliveryTime, that.mlDeliveryTime) &&
                Objects.equals(mlYardAreaId, that.mlYardAreaId) &&
                Objects.equals(mlYardLocId, that.mlYardLocId) &&
                Objects.equals(mlLicience, that.mlLicience) &&
                Objects.equals(mlLogId, that.mlLogId) &&
                Objects.equals(mlTruckTypeId, that.mlTruckTypeId) &&
                Objects.equals(mlTruckId, that.mlTruckId) &&
                Objects.equals(mlOtherDlr, that.mlOtherDlr) &&
                Objects.equals(pdiPlanArrivalDate, that.pdiPlanArrivalDate) &&
                Objects.equals(pdiPlanArrivalTime, that.pdiPlanArrivalTime) &&
                Objects.equals(pdiArrivalDate, that.pdiArrivalDate) &&
                Objects.equals(pdiArrivalTime, that.pdiArrivalTime) &&
                Objects.equals(pdiDelayReason, that.pdiDelayReason) &&
                Objects.equals(pdiVehicleSituationId, that.pdiVehicleSituationId) &&
                Objects.equals(pdiYardArea, that.pdiYardArea) &&
                Objects.equals(pdiLocId, that.pdiLocId) &&
                Objects.equals(pdiDeliveryDate, that.pdiDeliveryDate) &&
                Objects.equals(pdiDeliveryTime, that.pdiDeliveryTime) &&
                Objects.equals(pdiLogId, that.pdiLogId) &&
                Objects.equals(pdiTructTypeId, that.pdiTructTypeId) &&
                Objects.equals(pdiTructId, that.pdiTructId) &&
                Objects.equals(pdiTransCost, that.pdiTransCost) &&
                Objects.equals(pdiStockMaintenanceDate, that.pdiStockMaintenanceDate) &&
                Objects.equals(mlArrivalPlanDate, that.mlArrivalPlanDate) &&
                Objects.equals(mlArrivalPlanTime, that.mlArrivalPlanTime) &&
                Objects.equals(mlArrivalActualDate, that.mlArrivalActualDate) &&
                Objects.equals(mlArrivalActualTime, that.mlArrivalActualTime) &&
                Objects.equals(mlArrivalDelayReason, that.mlArrivalDelayReason) &&
                Objects.equals(mlArrivalSituationId, that.mlArrivalSituationId) &&
                Objects.equals(mlArrivalTransCost, that.mlArrivalTransCost) &&
                Objects.equals(dlrPlanArrivalDate, that.dlrPlanArrivalDate) &&
                Objects.equals(dlrPlanArrivalTime, that.dlrPlanArrivalTime) &&
                Objects.equals(dlrArrivalDate, that.dlrArrivalDate) &&
                Objects.equals(dlrArrivalTime, that.dlrArrivalTime) &&
                Objects.equals(dlrDelayReason, that.dlrDelayReason) &&
                Objects.equals(dlrTransCost, that.dlrTransCost) &&
                Objects.equals(dlrHasFloorMat, that.dlrHasFloorMat) &&
                Objects.equals(dlrGasoline, that.dlrGasoline) &&
                Objects.equals(dlrYardAreaId, that.dlrYardAreaId) &&
                Objects.equals(dlrYardLocId, that.dlrYardLocId) &&
                Objects.equals(damageIs, that.damageIs) &&
                Objects.equals(damageTypeId, that.damageTypeId) &&
                Objects.equals(damageRepaireCost, that.damageRepaireCost) &&
                Objects.equals(damagePlace, that.damagePlace) &&
                Objects.equals(damageReason, that.damageReason) &&
                Objects.equals(damageDes, that.damageDes) &&
                Objects.equals(damageInsCompanyId, that.damageInsCompanyId) &&
                Objects.equals(damageClaimDate, that.damageClaimDate) &&
                Objects.equals(damageFinishingDate, that.damageFinishingDate) &&
                Objects.equals(stockServiceMaintenanceDate, that.stockServiceMaintenanceDate) &&
                Objects.equals(stockRemark, that.stockRemark) &&
                Objects.equals(createdBy, that.createdBy) &&
                Objects.equals(createDate, that.createDate) &&
                Objects.equals(modifiedBy, that.modifiedBy) &&
                Objects.equals(modifyDate, that.modifyDate) &&
                Objects.equals(dlrReceive, that.dlrReceive) &&
                Objects.equals(dlrRemark, that.dlrRemark) &&
                Objects.equals(dlrStatus, that.dlrStatus) &&
                Objects.equals(mlPlanDeliveryDate, that.mlPlanDeliveryDate) &&
                Objects.equals(mlPlanDeliveryTime, that.mlPlanDeliveryTime) &&
                Objects.equals(pdiPlanDeliveryTime, that.pdiPlanDeliveryTime) &&
                Objects.equals(pdiPlanDeliveryDate, that.pdiPlanDeliveryDate) &&
                Objects.equals(dlrVehicleSituationId, that.dlrVehicleSituationId) &&
                Objects.equals(assignmentDate, that.assignmentDate) &&
                Objects.equals(paymentBy, that.paymentBy) &&
                Objects.equals(graProId, that.graProId) &&
                Objects.equals(transportRoute, that.transportRoute) &&
                Objects.equals(isExport, that.isExport) &&
                Objects.equals(seqNo, that.seqNo) &&
                Objects.equals(pdiConducting, that.pdiConducting) &&
                Objects.equals(fapId, that.fapId) &&
                Objects.equals(fleetCustomer, that.fleetCustomer) &&
                Objects.equals(vrDate, that.vrDate) &&
                Objects.equals(cbuDocument, that.cbuDocument) &&
                Objects.equals(isReport, that.isReport) &&
                Objects.equals(tfsProcess, that.tfsProcess) &&
                Objects.equals(salesDlrId, that.salesDlrId) &&
                Objects.equals(salesDate, that.salesDate) &&
                Objects.equals(salesStatus, that.salesStatus) &&
                Objects.equals(tfsAmount, that.tfsAmount) &&
                Objects.equals(pInstallDate, that.pInstallDate) &&
                Objects.equals(pInstallTime, that.pInstallTime) &&
                Objects.equals(installTime, that.installTime) &&
                Objects.equals(deferPayment, that.deferPayment) &&
                Objects.equals(pdiDate, that.pdiDate) &&
                Objects.equals(cbuLocation, that.cbuLocation) &&
                Objects.equals(cbuDocPlan, that.cbuDocPlan) &&
                Objects.equals(stDelivery, that.stDelivery) &&
                Objects.equals(pioStatus, that.pioStatus) &&
                Objects.equals(firstTimeMaintPlan, that.firstTimeMaintPlan) &&
                Objects.equals(firstTimeMaintAct, that.firstTimeMaintAct) &&
                Objects.equals(secondTimeMaintPlan, that.secondTimeMaintPlan) &&
                Objects.equals(secondTimeMaintAct, that.secondTimeMaintAct) &&
                Objects.equals(thirdTimeMaintPlan, that.thirdTimeMaintPlan) &&
                Objects.equals(thirdTimeMaintAct, that.thirdTimeMaintAct) &&
                Objects.equals(forthTimeMaintPlan, that.forthTimeMaintPlan) &&
                Objects.equals(forthTimeMaintAct, that.forthTimeMaintAct) &&
                Objects.equals(fifthTimeMaintPlan, that.fifthTimeMaintPlan) &&
                Objects.equals(fifthTimeMaintAct, that.fifthTimeMaintAct) &&
                Objects.equals(sixthTimeMaintPlan, that.sixthTimeMaintPlan) &&
                Objects.equals(sixthTimeMaintAct, that.sixthTimeMaintAct) &&
                Objects.equals(pioLackList, that.pioLackList) &&
                Objects.equals(stArrival, that.stArrival) &&
                Objects.equals(stLogId, that.stLogId) &&
                Objects.equals(paintInTime, that.paintInTime) &&
                Objects.equals(clDelivery, that.clDelivery) &&
                Objects.equals(clLogId, that.clLogId) &&
                Objects.equals(clTruckTypeId, that.clTruckTypeId) &&
                Objects.equals(clTruckId, that.clTruckId) &&
                Objects.equals(spctTruckId, that.spctTruckId) &&
                Objects.equals(spctTruckTypeId, that.spctTruckTypeId) &&
                Objects.equals(spctLogId, that.spctLogId) &&
                Objects.equals(spctDeliveryDate, that.spctDeliveryDate) &&
                Objects.equals(tmapPaperInvoiceNo, that.tmapPaperInvoiceNo) &&
                Objects.equals(tmapPaperInvoiceDate, that.tmapPaperInvoiceDate) &&
                Objects.equals(customStartPlan, that.customStartPlan) &&
                Objects.equals(vrCheckPlan, that.vrCheckPlan) &&
                Objects.equals(customCompletePlan, that.customCompletePlan) &&
                Objects.equals(customStartAct, that.customStartAct) &&
                Objects.equals(vrCheckAct, that.vrCheckAct) &&
                Objects.equals(customCompleteAct, that.customCompleteAct) &&
                Objects.equals(customActLt, that.customActLt) &&
                Objects.equals(coqReceiveDate, that.coqReceiveDate) &&
                Objects.equals(homologation, that.homologation) &&
                Objects.equals(marineModeId, that.marineModeId) &&
                Objects.equals(departPortId, that.departPortId) &&
                Objects.equals(arivalPortId, that.arivalPortId) &&
                Objects.equals(portEtd, that.portEtd) &&
                Objects.equals(portEta, that.portEta) &&
                Objects.equals(actDepartDate, that.actDepartDate) &&
                Objects.equals(actArrivalDate, that.actArrivalDate) &&
                Objects.equals(vrCheckActLt, that.vrCheckActLt) &&
                Objects.equals(interPortAssignDate, that.interPortAssignDate) &&
                Objects.equals(interAssignDesId, that.interAssignDesId) &&
                Objects.equals(interPortDispatchDate, that.interPortDispatchDate) &&
                Objects.equals(interTransRouteId, that.interTransRouteId) &&
                Objects.equals(interMeanTransId, that.interMeanTransId) &&
                Objects.equals(interLogId, that.interLogId) &&
                Objects.equals(interTructId, that.interTructId) &&
                Objects.equals(interTransCost, that.interTransCost) &&
                Objects.equals(interDesArrivalDate, that.interDesArrivalDate) &&
                Objects.equals(interDesArrivalTime, that.interDesArrivalTime) &&
                Objects.equals(swapAssignDate, that.swapAssignDate) &&
                Objects.equals(swapAssignDesId, that.swapAssignDesId) &&
                Objects.equals(swapRouteId, that.swapRouteId) &&
                Objects.equals(swapReason, that.swapReason) &&
                Objects.equals(swapLogId, that.swapLogId) &&
                Objects.equals(swapTructTypeId, that.swapTructTypeId) &&
                Objects.equals(swapTructId, that.swapTructId) &&
                Objects.equals(swapTransportCost, that.swapTransportCost) &&
                Objects.equals(swapDesArrivalDate, that.swapDesArrivalDate) &&
                Objects.equals(orderMonth, that.orderMonth) &&
                Objects.equals(productionDate, that.productionDate) &&
                Objects.equals(dlrDispatchPlan, that.dlrDispatchPlan) &&
                Objects.equals(dlrLogId, that.dlrLogId) &&
                Objects.equals(dlrTruckTypeId, that.dlrTruckTypeId) &&
                Objects.equals(dlrTruckId, that.dlrTruckId) &&
                Objects.equals(dlrTransRouteId, that.dlrTransRouteId) &&
                Objects.equals(stockDay, that.stockDay) &&
                Objects.equals(stockFeeExport, that.stockFeeExport) &&
                Objects.equals(interiorColId, that.interiorColId) &&
                Objects.equals(vehicleStatus, that.vehicleStatus) &&
                Objects.equals(estimateAllocationMonth, that.estimateAllocationMonth) &&
                Objects.equals(remark, that.remark) &&
                Objects.equals(customTmvArrDate, that.customTmvArrDate) &&
                Objects.equals(interPortDispatchPlan, that.interPortDispatchPlan) &&
                Objects.equals(interPortDispatchDelay, that.interPortDispatchDelay) &&
                Objects.equals(interPortDelayReason, that.interPortDelayReason) &&
                Objects.equals(interDesArrivalDelay, that.interDesArrivalDelay) &&
                Objects.equals(interDesArrivalPlan, that.interDesArrivalPlan) &&
                Objects.equals(interDesArrivalDelayReason, that.interDesArrivalDelayReason) &&
                Objects.equals(swappingDispatchPlan, that.swappingDispatchPlan) &&
                Objects.equals(swappingDispatchDate, that.swappingDispatchDate) &&
                Objects.equals(swappingDesArrivalPlan, that.swappingDesArrivalPlan) &&
                Objects.equals(swappingArrivalDelay, that.swappingArrivalDelay) &&
                Objects.equals(swappingArrivalDelayReason, that.swappingArrivalDelayReason) &&
                Objects.equals(dlrAssPlanDate, that.dlrAssPlanDate) &&
                Objects.equals(dlrAssActualDate, that.dlrAssActualDate) &&
                Objects.equals(pdiOwnerManual, that.pdiOwnerManual) &&
                Objects.equals(damageFoundDate, that.damageFoundDate) &&
                Objects.equals(mlDelayDispatchReason, that.mlDelayDispatchReason) &&
                Objects.equals(mlDelayDispatch, that.mlDelayDispatch) &&
                Objects.equals(pdiTransportRoute, that.pdiTransportRoute) &&
                Objects.equals(damageRepairDate, that.damageRepairDate) &&
                Objects.equals(dlrDispatchDate, that.dlrDispatchDate) &&
                Objects.equals(dlrDispatchTime, that.dlrDispatchTime) &&
                Objects.equals(blNo, that.blNo) &&
                Objects.equals(customSheetNo, that.customSheetNo) &&
                Objects.equals(priceConsultingDate, that.priceConsultingDate) &&
                Objects.equals(tmvOcSigningDate, that.tmvOcSigningDate) &&
                Objects.equals(customOcSigningDate, that.customOcSigningDate) &&
                Objects.equals(customRemark, that.customRemark) &&
                Objects.equals(invoiceRequestDate, that.invoiceRequestDate) &&
                Objects.equals(dlrDelay, that.dlrDelay) &&
                Objects.equals(vrCertificateReceiveDate, that.vrCertificateReceiveDate) &&
                Objects.equals(cbuProductionMonth, that.cbuProductionMonth) &&
                Objects.equals(originalLoPlanDate, that.originalLoPlanDate) &&
                Objects.equals(loDelayReason, that.loDelayReason) &&
                Objects.equals(dlrActualArrivalDate, that.dlrActualArrivalDate) &&
                Objects.equals(originalArrivalPlan, that.originalArrivalPlan) &&
                Objects.equals(delayReasonOriArrPlan, that.delayReasonOriArrPlan) &&
                Objects.equals(docSendingVendor, that.docSendingVendor) &&
                Objects.equals(swapRemark, that.swapRemark) &&
                Objects.equals(yardAreaId, that.yardAreaId) &&
                Objects.equals(yardLocId, that.yardLocId) &&
                Objects.equals(originalLoPlanTime, that.originalLoPlanTime) &&
                Objects.equals(tempLicenseDate, that.tempLicenseDate) &&
                Objects.equals(diesel, that.diesel) &&
                Objects.equals(transportWay, that.transportWay) &&
                Objects.equals(portTransportWay, that.portTransportWay) &&
                Objects.equals(dlrTransportWay, that.dlrTransportWay) &&
                Objects.equals(dlrDispatchPlanTime, that.dlrDispatchPlanTime) &&
                Objects.equals(sideAirbagLh, that.sideAirbagLh) &&
                Objects.equals(sideAirbagRh, that.sideAirbagRh) &&
                Objects.equals(kneeAirbag, that.kneeAirbag) &&
                Objects.equals(seqLexus, that.seqLexus) &&
                Objects.equals(selfDrivingTripRequest, that.selfDrivingTripRequest) &&
                Objects.equals(modifiedDateDispatchPlan, that.modifiedDateDispatchPlan) &&
                Objects.equals(dlrRqColId, that.dlrRqColId) &&
                Objects.equals(dlrColDeadline, that.dlrColDeadline) &&
                Objects.equals(tfsCreateDate, that.tfsCreateDate) &&
                Objects.equals(tmssNo, that.tmssNo) &&
                Objects.equals(latestLoPlanDate, that.latestLoPlanDate) &&
                Objects.equals(modifyDateAndon, that.modifyDateAndon) &&
                Objects.equals(engCode, that.engCode) &&
                Objects.equals(dlrAtId, that.dlrAtId) &&
                Objects.equals(payStatus, that.payStatus) &&
                Objects.equals(modifyDateYardSystem, that.modifyDateYardSystem) &&
                Objects.equals(truckLocation, that.truckLocation) &&
                Objects.equals(standbyLocationPdi, that.standbyLocationPdi) &&
                Objects.equals(mlMovingYardDate, that.mlMovingYardDate) &&
                Objects.equals(mlMovingYardTime, that.mlMovingYardTime) &&
                Objects.equals(driverName, that.driverName) &&
                Objects.equals(modifiedDateRqColId, that.modifiedDateRqColId) &&
                Objects.equals(modifyDateColDeadline, that.modifyDateColDeadline) &&
                Objects.equals(pdiTime, that.pdiTime) &&
                Objects.equals(pBeginInstallTime, that.pBeginInstallTime) &&
                Objects.equals(pioBay, that.pioBay) &&
                Objects.equals(pioMember, that.pioMember) &&
                Objects.equals(dispatchChangeReqDate, that.dispatchChangeReqDate) &&
                Objects.equals(dispatchChangeSendDate, that.dispatchChangeSendDate) &&
                Objects.equals(advanceRequestDate, that.advanceRequestDate) &&
                Objects.equals(dispatchChangeStatus, that.dispatchChangeStatus) &&
                Objects.equals(newDispatchPlanDate, that.newDispatchPlanDate) &&
                Objects.equals(dispatchChangeConfirmDate, that.dispatchChangeConfirmDate) &&
                Objects.equals(cbuDocDelivery, that.cbuDocDelivery) &&
                Objects.equals(printInvDescAndon, that.printInvDescAndon) &&
                Objects.equals(pFinishInstallTime, that.pFinishInstallTime);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, dlrId, frameNo, vin, engineNo, accId, cfuId, graId, locId, truId, installDate, painInDate, ccrLotNo, ccrNoInLot, ccrBodyNo, ccrAIn, ccrAOut, keyCode, qcDriverNo, qcPassengerNo, qcEngno, qcTranmissionNo, qcAxleNo, qcLoshift, lineOffDate, assChangeFrom, assChangeReason, assAlloMonth, dlrPaymentPlan, rFPaymentDate, payInvoiceNo, payActualInvoiceDate, payVnAmount, payUsdAmount, payHoldbackAmount, payRFPayback, payDiscountAmount, documentTmvSituationId, documentDeliveryDate, documentDeliveryTime, documentArrivalDate, documentArrivalTime, documentArrivalRemark, documentDlrSituationId, fleetPrice, mlDeliveryDate, mlDeliveryTime, mlYardAreaId, mlYardLocId, mlLicience, mlLogId, mlTruckTypeId, mlTruckId, mlOtherDlr, pdiPlanArrivalDate, pdiPlanArrivalTime, pdiArrivalDate, pdiArrivalTime, pdiDelayReason, pdiVehicleSituationId, pdiYardArea, pdiLocId, pdiDeliveryDate, pdiDeliveryTime, pdiLogId, pdiTructTypeId, pdiTructId, pdiTransCost, pdiStockMaintenanceDate, mlArrivalPlanDate, mlArrivalPlanTime, mlArrivalActualDate, mlArrivalActualTime, mlArrivalDelayReason, mlArrivalSituationId, mlArrivalTransCost, dlrPlanArrivalDate, dlrPlanArrivalTime, dlrArrivalDate, dlrArrivalTime, dlrDelayReason, dlrTransCost, dlrHasFloorMat, dlrGasoline, dlrYardAreaId, dlrYardLocId, damageIs, damageTypeId, damageRepaireCost, damagePlace, damageReason, damageDes, damageInsCompanyId, damageClaimDate, damageFinishingDate, stockServiceMaintenanceDate, stockRemark, createdBy, createDate, modifiedBy, modifyDate, dlrReceive, dlrRemark, dlrStatus, mlPlanDeliveryDate, mlPlanDeliveryTime, pdiPlanDeliveryTime, pdiPlanDeliveryDate, dlrVehicleSituationId, assignmentDate, paymentBy, graProId, transportRoute, isExport, seqNo, pdiConducting, fapId, fleetCustomer, vrDate, cbuDocument, isReport, tfsProcess, salesDlrId, salesDate, salesStatus, tfsAmount, pInstallDate, pInstallTime, installTime, deferPayment, pdiDate, cbuLocation, cbuDocPlan, stDelivery, pioStatus, firstTimeMaintPlan, firstTimeMaintAct, secondTimeMaintPlan, secondTimeMaintAct, thirdTimeMaintPlan, thirdTimeMaintAct, forthTimeMaintPlan, forthTimeMaintAct, fifthTimeMaintPlan, fifthTimeMaintAct, sixthTimeMaintPlan, sixthTimeMaintAct, pioLackList, stArrival, stLogId, paintInTime, clDelivery, clLogId, clTruckTypeId, clTruckId, spctTruckId, spctTruckTypeId, spctLogId, spctDeliveryDate, tmapPaperInvoiceNo, tmapPaperInvoiceDate, customStartPlan, vrCheckPlan, customCompletePlan, customStartAct, vrCheckAct, customCompleteAct, customActLt, coqReceiveDate, homologation, marineModeId, departPortId, arivalPortId, portEtd, portEta, actDepartDate, actArrivalDate, vrCheckActLt, interPortAssignDate, interAssignDesId, interPortDispatchDate, interTransRouteId, interMeanTransId, interLogId, interTructId, interTransCost, interDesArrivalDate, interDesArrivalTime, swapAssignDate, swapAssignDesId, swapRouteId, swapReason, swapLogId, swapTructTypeId, swapTructId, swapTransportCost, swapDesArrivalDate, orderMonth, productionDate, dlrDispatchPlan, dlrLogId, dlrTruckTypeId, dlrTruckId, dlrTransRouteId, stockDay, stockFeeExport, interiorColId, vehicleStatus, estimateAllocationMonth, remark, customTmvArrDate, interPortDispatchPlan, interPortDispatchDelay, interPortDelayReason, interDesArrivalDelay, interDesArrivalPlan, interDesArrivalDelayReason, swappingDispatchPlan, swappingDispatchDate, swappingDesArrivalPlan, swappingArrivalDelay, swappingArrivalDelayReason, dlrAssPlanDate, dlrAssActualDate, pdiOwnerManual, damageFoundDate, mlDelayDispatchReason, mlDelayDispatch, pdiTransportRoute, damageRepairDate, dlrDispatchDate, dlrDispatchTime, blNo, customSheetNo, priceConsultingDate, tmvOcSigningDate, customOcSigningDate, customRemark, invoiceRequestDate, dlrDelay, vrCertificateReceiveDate, cbuProductionMonth, originalLoPlanDate, loDelayReason, dlrActualArrivalDate, originalArrivalPlan, delayReasonOriArrPlan, docSendingVendor, swapRemark, yardAreaId, yardLocId, originalLoPlanTime, tempLicenseDate, diesel, transportWay, portTransportWay, dlrTransportWay, dlrDispatchPlanTime, sideAirbagLh, sideAirbagRh, kneeAirbag, seqLexus, selfDrivingTripRequest, modifiedDateDispatchPlan, dlrRqColId, dlrColDeadline, tfsCreateDate, tmssNo, latestLoPlanDate, modifyDateAndon, engCode, dlrAtId, payStatus, modifyDateYardSystem, truckLocation, standbyLocationPdi, mlMovingYardDate, mlMovingYardTime, driverName, modifiedDateRqColId, modifyDateColDeadline, pdiTime, pBeginInstallTime, pioBay, pioMember, dispatchChangeReqDate, dispatchChangeSendDate, advanceRequestDate, dispatchChangeStatus, newDispatchPlanDate, dispatchChangeConfirmDate, cbuDocDelivery, printInvDescAndon, pFinishInstallTime);
    }
}
